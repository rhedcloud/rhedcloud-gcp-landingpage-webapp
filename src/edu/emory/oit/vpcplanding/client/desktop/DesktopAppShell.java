package edu.emory.oit.vpcplanding.client.desktop;

import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Element;
import com.google.gwt.dom.client.Style;
import com.google.gwt.dom.client.Style.BorderStyle;
import com.google.gwt.dom.client.Style.FontWeight;
import com.google.gwt.dom.client.Style.TextAlign;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.event.dom.client.KeyDownEvent;
import com.google.gwt.event.dom.client.KeyDownHandler;
import com.google.gwt.event.dom.client.MouseOverEvent;
import com.google.gwt.event.dom.client.MouseOverHandler;
import com.google.gwt.event.logical.shared.CloseEvent;
import com.google.gwt.event.logical.shared.CloseHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.Command;
import com.google.gwt.user.client.Event;
import com.google.gwt.user.client.EventListener;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DeckLayoutPanel;
import com.google.gwt.user.client.ui.DockPanel;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.HasVerticalAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.IsWidget;
import com.google.gwt.user.client.ui.MenuBar;
import com.google.gwt.user.client.ui.MenuItem;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.PushButton;
import com.google.gwt.user.client.ui.ResizeComposite;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import com.google.web.bindery.event.shared.EventBus;
import edu.emory.oit.vpcplanding.client.AppShell;
import edu.emory.oit.vpcplanding.client.ClientFactory;
import edu.emory.oit.vpcplanding.client.VpcpLandingService;
import edu.emory.oit.vpcplanding.client.common.VpcpAlert;
import edu.emory.oit.vpcplanding.shared.AWSServicePojo;
import edu.emory.oit.vpcplanding.shared.AWSServiceQueryFilterPojo;
import edu.emory.oit.vpcplanding.shared.AWSServiceQueryResultPojo;
import edu.emory.oit.vpcplanding.shared.AWSServiceStatisticPojo;
import edu.emory.oit.vpcplanding.shared.AWSServiceSummaryPojo;
import edu.emory.oit.vpcplanding.shared.PropertiesPojo;
import edu.emory.oit.vpcplanding.shared.ReleaseInfo;
import edu.emory.oit.vpcplanding.shared.SecurityRiskPojo;
import edu.emory.oit.vpcplanding.shared.ServiceSecurityAssessmentPojo;
import edu.emory.oit.vpcplanding.shared.ServiceSecurityAssessmentQueryFilterPojo;
import edu.emory.oit.vpcplanding.shared.ServiceSecurityAssessmentQueryResultPojo;
import edu.emory.oit.vpcplanding.shared.UserAccountPojo;

public class DesktopAppShell extends ResizeComposite implements AppShell {
	PopupPanel pleaseWaitDialog;
//	PopupPanel hamburgerPopup;

    Logger log=Logger.getLogger(DesktopAppShell.class.getName());
    ClientFactory clientFactory;
    EventBus eventBus;
	AWSServiceSummaryPojo serviceSummary;
	boolean refreshingServices=false;
	boolean leavingPage=false;
    String siteSpecificServiceName = null;
    String siteName = null;
    PropertiesPojo siteSpecificProperties;

	int panelNumber=0;
    List<DockPanel> carouselPanels = new java.util.ArrayList<DockPanel>();
    int numberOfDockPanels = 0;
    int panelCounter = 0;
    boolean firstWestClick=true;
    boolean firstEastClick=true;
    boolean b_break=false;
    
	private static DesktopAppShellUiBinder uiBinder = GWT.create(DesktopAppShellUiBinder.class);

	interface DesktopAppShellUiBinder extends UiBinder<Widget, DesktopAppShell> {
	}

	public DesktopAppShell() {
		GWT.log("DESKTOP SHELL:  default");
		initWidget(uiBinder.createAndBindUi(this));
	}
	public DesktopAppShell(final EventBus eventBus, ClientFactory clientFactory) {
		GWT.log("GWT.moduleBaseURL: " + GWT.getModuleBaseURL());
		GWT.log("DESKTOP SHELL:  event bus and client factory");
		initWidget(uiBinder.createAndBindUi(this));
		
		this.clientFactory = clientFactory;
		this.eventBus = eventBus;
		leavingPage=false;
		
		AsyncCallback<PropertiesPojo> sst_cb = new AsyncCallback<PropertiesPojo>() {
			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
			}

			@Override
			public void onSuccess(PropertiesPojo result) {
				String defaultSiteName = "Emory";
				String defaultServiceName = "AWS at Emory";
				if (result != null) {
					siteSpecificServiceName = result.getProperty("siteServiceName", defaultServiceName);
					siteName = result.getProperty("siteName", defaultSiteName);
				}
				else {
					siteSpecificServiceName = defaultServiceName;
					siteName = defaultSiteName;
				}
				exploreHTML.setHTML(exploreHTML.getHTML().replaceAll("SITE_SERVICE_NAME", siteSpecificServiceName));;
				trainingHtmlPanel.setTitle(trainingHtmlPanel.getTitle().replaceAll("SITE_SERVICE_NAME", siteSpecificServiceName));
				fundingHtmlPanel.setTitle(trainingHtmlPanel.getTitle().replaceAll("SITE_SERVICE_NAME", siteSpecificServiceName));
				otherResourcesHtmlPanel.setTitle(trainingHtmlPanel.getTitle().replaceAll("SITE_SERVICE_NAME", siteSpecificServiceName));
			}
		};
		VpcpLandingService.Util.getInstance().getSiteSpecificTextProperties(sst_cb);
		
		initMenus();
		
		String s_break = com.google.gwt.user.client.Window.Location.getParameter("breakTest");
		if (s_break != null) {
			b_break = Boolean.parseBoolean(s_break);
//			if (b_break) {
//				GWT.log("break the test, breakTest=true");
//				emoryKbItem.setText("broken");
//				awsDocumentationItem.setText("broken");
//				demoItem.setText("broken");
//				contactAwsItem.setText("broken");;
//				emoryAwsItem.setText("broken");;
//				esbServiceStatusItem.setText("broken");
//				monitoringStatusItem.setText("broken");
//			}
//			else {
//				GWT.log("DON'T break the test, breakTest != true");
//			}
		}
		else {
			GWT.log("DON'T break the test, breakTest=null");
		}

		AsyncCallback<ReleaseInfo> riCallback = new AsyncCallback<ReleaseInfo>() {
			@Override
			public void onFailure(Throwable caught) {
				GWT.log("Error getting release info", caught);
		        releaseInfoElem.setInnerHTML("Error getting release info");
			}

			@Override
			public void onSuccess(ReleaseInfo result) {
		        releaseInfoElem.setInnerHTML(result.toString());
			}
		};
		VpcpLandingService.Util.getInstance().getReleaseInfo(riCallback);
		
//		AsyncCallback<UserAccountPojo> userCallback = new AsyncCallback<UserAccountPojo>() {
//			@Override
//			public void onFailure(Throwable caught) {
//				GWT.log("problem getting user logged in..." + caught.getMessage(), caught);
//			}
//
//			@Override
//			public void onSuccess(UserAccountPojo result) {
//				if (result != null) {
//					userNameElem.setInnerHTML(result.getEppn());
//				}
//			}
//		};
//		VpcpLandingService.Util.getInstance().getUserLoggedIn(userCallback);

        registerEvents();
        
        AsyncCallback<List<PropertiesPojo>> cp_cb = new AsyncCallback<List<PropertiesPojo>>() {
			@Override
			public void onFailure(Throwable caught) {
				GWT.log("Exception getting carouselproperties", caught);
			}

			@Override
			public void onSuccess(List<PropertiesPojo> result) {
				GWT.log("Got " + result.size() + " carouselproperties back from the service layer");
				numberOfDockPanels = result.size();
				
				// TODO: probably need a default carousel if there aren't any carouselProperties returned
				// So, default imageURL and displayText at least
				
				for (PropertiesPojo pp : result) {
					// build a panel based on content in this properties object.
					GWT.log("sortOrder is: " + pp.getProperty("sortOrder"));
					
					// TODO: handle null
					final String imageURL = pp.getProperty("imageURL", null);
					GWT.log("imageURL is: " + imageURL);
					
					final String imageTarget = pp.getProperty("imageTarget", null);
					GWT.log("imageTarget is: " + imageTarget);
					
					// TODO: handle null
					final String displayText = pp.getProperty("displayText", null);
					GWT.log("displayText is: " + displayText);
					
					DockPanel dock = new DockPanel();
					dock.setSpacing(4);
					dock.setHorizontalAlignment(DockPanel.ALIGN_CENTER);
					dock.setVerticalAlignment(DockPanel.ALIGN_MIDDLE);
					dock.setHeight("100%");
					dock.setWidth("99%");
					dock.setStyleName("cw-DockPanel");
					
					VerticalPanel westVP = new VerticalPanel();
					westVP.setWidth("150px");
					westVP.setHeight("100%");
//					westVP.getElement().getStyle().setBackgroundImage("linear-gradient(to right,rgba(0,40,120,1) 0,rgba(0,40,120,.005) 100%)");
					Anchor westAnchor = new Anchor("<");
//					westAnchor.getElement().getStyle().setBackgroundImage("linear-gradient(to right,rgba(0,40,120,1) 0,rgba(0,40,120,.005) 100%)");
//					westAnchor.getElement().getStyle().setColor("white");
					westAnchor.getElement().getStyle().setFontWeight(FontWeight.BOLDER);
					westAnchor.getElement().getStyle().setFontSize(25, Unit.PX);
					westVP.add(westAnchor);
					westVP.setCellHorizontalAlignment(westAnchor, HasHorizontalAlignment.ALIGN_CENTER);
					westVP.setCellVerticalAlignment(westAnchor, HasVerticalAlignment.ALIGN_MIDDLE);
					westAnchor.addStyleName("carouselControl");
					westAnchor.addClickHandler(new ClickHandler() {
						@Override
						public void onClick(ClickEvent event) {
							stopCarouselTimer();
							GWT.log("[west:1] panelCounter is: " + panelCounter);
							if (panelCounter <= 0) {
								panelCounter = numberOfDockPanels - 1;
							}
							else {
								if (!firstWestClick) {
									panelCounter--;
								}
							}
							firstWestClick = false;
							GWT.log("[west:3] panelCounter is: " + panelCounter);
							DockPanel dock = carouselPanels.get(panelCounter);
							carouselContainer.setWidget(dock);
							carouselContainer.animate(2000);
						}
					});
//					dock.add(westAnchor, DockPanel.WEST);
					dock.add(westVP, DockPanel.WEST);
					
					
					VerticalPanel eastVP = new VerticalPanel();
					eastVP.setWidth("150px");
					eastVP.setHeight("100%");
//					eastVP.getElement().getStyle().setBackgroundImage("linear-gradient(to right,rgba(0,40,120,1) 0,rgba(0,40,120,.005) 100%)");
					Anchor eastAnchor = new Anchor(">");
//					eastAnchor.getElement().getStyle().setBackgroundImage("linear-gradient(to right,rgba(0,40,120,1) 0,rgba(0,40,120,.005) 100%)");
//					eastAnchor.getElement().getStyle().setColor("white");
					eastAnchor.getElement().getStyle().setFontWeight(FontWeight.BOLDER);
					eastAnchor.getElement().getStyle().setFontSize(25, Unit.PX);
					eastVP.add(eastAnchor);
					eastVP.setCellHorizontalAlignment(eastAnchor, HasHorizontalAlignment.ALIGN_CENTER);
					eastVP.setCellVerticalAlignment(eastAnchor, HasVerticalAlignment.ALIGN_MIDDLE);
					eastAnchor.addStyleName("carouselControl");
					eastAnchor.addClickHandler(new ClickHandler() {
						@Override
						public void onClick(ClickEvent event) {
							stopCarouselTimer();
							GWT.log("[east:1] panelCounter is: " + panelCounter);
							if (panelCounter >= numberOfDockPanels - 1) {
								panelCounter = 0;
							}
							else {
								panelCounter++;
							}
							GWT.log("[east:3] panelCounter is: " + panelCounter);
							DockPanel dock = carouselPanels.get(panelCounter);
							carouselContainer.setWidget(dock);
							carouselContainer.animate(2000);
						}
					});
//					dock.add(eastAnchor, DockPanel.EAST);
					dock.add(eastVP, DockPanel.EAST);
					
					Image image = new Image(imageURL);
					image.setHeight("25.0em");
//					image.setWidth("95%");
					image.setWidth("70%");
					image.addStyleName("carouselImage");
					if (imageTarget != null) {
						image.getElement().getStyle().setCursor(Style.Cursor.POINTER);
					}
					dock.add(image, DockPanel.CENTER);
					image.addClickHandler(new ClickHandler() {
						@Override
						public void onClick(ClickEvent event) {
							stopCarouselTimer();
							if (imageTarget != null) {
								GWT.log("opening " + imageTarget);
								Window.open(imageTarget, "_blank", "");
							}
							else {
								GWT.log("imageTarget is null");
							}
						}
					});
					
					HTML text = new HTML(displayText);
					text.getElement().getStyle().setFontSize(2.5, Unit.EM);
					text.getElement().getStyle().setColor("#002878");
					text.getElement().getStyle().setFontWeight(FontWeight.BOLD);
					text.getElement().getStyle().setTextAlign(TextAlign.CENTER);
					text.getElement().getStyle().setLineHeight(2.0, Unit.EM);
					if (imageTarget != null) {
						text.getElement().getStyle().setCursor(Style.Cursor.POINTER);
						text.addClickHandler(new ClickHandler() {
							@Override
							public void onClick(ClickEvent event) {
								stopCarouselTimer();
								Window.open(imageTarget, "_blank", "");
							}
						});
					}
					
					dock.add(text, DockPanel.SOUTH);
					
					carouselPanels.add(dock);
					carouselContainer.add(dock);
				}
				DockPanel dock = carouselPanels.get(0);
				carouselContainer.setWidget(dock);
				carouselContainer.animate(0);
				panelCounter = 0;
				startCarouselTimer(5000);
				carouselTimer.scheduleRepeating(5000);
			}
        	
        };
        VpcpLandingService.Util.getInstance().getCarouselProperties(cp_cb);
	}
	
	@UiHandler("productsHTML")
	void productsHTMLClicked(ClickEvent e) {
		// TODO: build the products menu and show it somehow
		// Products
		// - all
		// - hipaa
		// - standard
		// Downloads
		// - tkiclient
		
		MenuBar menu = new MenuBar(true);
		menu.setAnimationEnabled(true);
		menu.setAutoOpen(true);
		menu.addStyleName("linksMenu");
		
		MenuBar productsMenu = new MenuBar(true);
		productsMenu.setAnimationEnabled(true);
		productsMenu.addStyleName("linksMenuHeader");
		
		MenuItem allServicesItem = new MenuItem("Search All Services", productsCommand);
		allServicesItem.addStyleName("linksMenuItem");

		MenuItem hipaaServicesItem = new MenuItem("Available HIPAA Project Services", hipaaServicesCommand);
		allServicesItem.addStyleName("linksMenuItem");

		MenuItem standardServicesItem = new MenuItem("Available Standard Project Services", standardServicesCommand);
		allServicesItem.addStyleName("linksMenuItem");

		productsMenu.addItem(allServicesItem);
		productsMenu.addItem(hipaaServicesItem);
		productsMenu.addItem(standardServicesItem);
		
		menu.addItem(new MenuItem("Products", productsMenu));
		
		MenuBar downloadsMenu = new MenuBar(true);
		downloadsMenu.setAnimationEnabled(true);
		
		PopupPanel pp = new PopupPanel(true);
		pp.setWidget(menu);
		pp.showRelativeTo(productsHTML);
	}
	
	@UiHandler("helpHTML")
	void helpHTMLClicked(ClickEvent e) {
		// TODO: build the help menu and show it somehow
		// Documentation
		// - Knowledge base
		// - GCP Documentation
		// - Demos
		// Support
		// - Contact GCP Support
		// - Contact Site Support
		// - Check System Status
		
		MenuBar menu = new MenuBar(true);
		menu.setAnimationEnabled(true);
		menu.setAutoOpen(true);
		menu.addStyleName("linksMenu");
		
		MenuBar documentationMenu = new MenuBar(true);
		documentationMenu.setAnimationEnabled(true);
		documentationMenu.addStyleName("linksMenuHeader");
		
		MenuItem knowledgBaseItem = new MenuItem("Knowledge Base", knowledgeBaseCommand);
		knowledgBaseItem.addStyleName("linksMenuItem");
		if (b_break) {
			knowledgBaseItem.setText("broken");
		}

		MenuItem gcpDocumentationItem = new MenuItem("GCP Documentation", hipaaServicesCommand);
		gcpDocumentationItem.addStyleName("linksMenuItem");
		if (b_break) {
			gcpDocumentationItem.setText("broken");
		}

		MenuItem demosItem = new MenuItem("Demos", demosCommand);
		demosItem.addStyleName("linksMenuItem");
		if (b_break) {
			demosItem.setText("broken");
		}

		documentationMenu.addItem(knowledgBaseItem);
		documentationMenu.addItem(gcpDocumentationItem);
		documentationMenu.addItem(demosItem);
		
		menu.addItem(new MenuItem("Documentation", documentationMenu));
		
		MenuBar supportMenu = new MenuBar(true);
		supportMenu.setAnimationEnabled(true);
		
		MenuItem contactGcpSupportItem = new MenuItem("Contact GCP Support", gcpSupportCommand);
		contactGcpSupportItem.addStyleName("linksMenuItem");
		if (b_break) {
			contactGcpSupportItem.setText("broken");
		}

		MenuItem contactSiteSupportItem = new MenuItem("Contact Site Support", siteSupportCommand);
		contactSiteSupportItem.addStyleName("linksMenuItem");
		if (b_break) {
			contactSiteSupportItem.setText("broken");
		}

		MenuItem systemStatusItem = new MenuItem("Check System Status", esbServiceStatusCommand);
		systemStatusItem.addStyleName("linksMenuItem");
		if (b_break) {
			systemStatusItem.setText("broken");
		}
		
		supportMenu.addItem(contactGcpSupportItem);
		supportMenu.addItem(contactSiteSupportItem);
		supportMenu.addItem(systemStatusItem);
		
		menu.addItem(new MenuItem("Support", supportMenu));
		
		PopupPanel pp = new PopupPanel(true);
		pp.setWidget(menu);
		pp.showRelativeTo(productsHTML);
	}

    final Timer carouselTimer = new Timer() {
		@Override
		public void run() {
			DockPanel dock = carouselPanels.get(panelCounter);
			carouselContainer.setWidget(dock);
			if (panelCounter >= numberOfDockPanels - 1) {
				panelCounter = 0;
			}
			else {
				panelCounter++;
			}
			carouselContainer.animate(2000);
		}
	};


	void startCarouselTimer(int repeateMillis) {
		if (carouselTimer != null) {
			carouselTimer.scheduleRepeating(repeateMillis);
		}
	}
	void stopCarouselTimer() {
		if (carouselTimer != null) {
			carouselTimer.cancel();
		}
	}

//	@UiField Image hamburgerMenu;
	@UiField VerticalPanel logoPanel;
	
	@UiField HorizontalPanel generalInfoPanel;
	@UiField HorizontalPanel linksPanel;
	@UiField Element releaseInfoElem;
//	@UiField Element userNameElem;
	
	@UiField Element serviceOverviewImageElem;
	@UiField Element serviceOverviewElem;
	@UiField Element useCasesImageElem;
	@UiField Element useCasesElem;
	@UiField Element gettingStartedImageElem;
	@UiField Element gettingStartedElem;
	
	@UiField Element faqImageElem;
	@UiField Element securityImageElem;
	@UiField Element supportImageElem;
	@UiField Element architectureImageElem;
	@UiField Element partnersImageElem;
	@UiField Element grantsImageElem;
	@UiField Element trainingImageElem;
	@UiField Element fundingImageElem;
	@UiField Element resourcesImageElem;

	@UiField Button awsConsoleButton;
	@UiField Button emoryConsoleButton;
	@UiField Button createAccountButton;
//	@UiField MenuBar documentationMenuBar;
//	@UiField MenuItem documentationMenu;
//	@UiField MenuItem esbServiceStatusItem;
//	@UiField MenuItem monitoringStatusItem;
//	
//	@UiField MenuItem emoryKbItem;
//	@UiField MenuItem awsDocumentationItem;
//	@UiField MenuItem demoItem;
//	@UiField MenuItem contactAwsItem;
//	@UiField MenuItem emoryAwsItem;
//	@UiField MenuItem awsItem;
//	
//	@UiField MenuItem productsItem;
//	@UiField MenuItem hipaaServicesItem;
//	@UiField MenuItem standardServicesItem;
	@UiField VerticalPanel serviceListPanel;
	@UiField VerticalPanel contentPanel;
	@UiField Element logoElem;
	
	@UiField HTML exploreHTML;
	@UiField HTMLPanel trainingHtmlPanel;
	@UiField HTMLPanel fundingHtmlPanel;
	@UiField HTMLPanel otherResourcesHtmlPanel;

//	@UiField HTML userInfoHTML;
	@UiField HTML productsHTML;
	@UiField HTML helpHTML;

	Command knowledgeBaseCommand;
	Command demosCommand;
	Command gcpSupportCommand;
	Command siteSupportCommand;

	void initMenus() {
		AsyncCallback<PropertiesPojo> kb_cb = new AsyncCallback<PropertiesPojo>() {
			@Override
			public void onFailure(Throwable caught) {
				GWT.log("Exception Menu Properties", caught);
				showMessageToUser("Exception getting menu properties, menus won't work:  " + caught.getMessage());
			}

			@Override
			public void onSuccess(PropertiesPojo result) {
//				emoryKbItem.setTitle(result.getProperty("title", "Unknown"));
//				emoryKbItem.setText(result.getProperty("text", "Unknown"));
				final String target = result.getProperty("target", null);
				final String href = result.getProperty("href", "Unknown");
				knowledgeBaseCommand = new Command() {
					@Override
					public void execute() {
						if (target != null && target.equalsIgnoreCase("_blank")) {
							Window.open(href, "_blank", "");
						}
						else {
							Window.Location.assign(href);
						}
					}
				};
			}
		};
		VpcpLandingService.Util.getInstance().getPropertiesForMenu("kbItem", kb_cb);

		AsyncCallback<PropertiesPojo> demo_cb = new AsyncCallback<PropertiesPojo>() {
			@Override
			public void onFailure(Throwable caught) {
				GWT.log("Exception Menu Properties", caught);
				showMessageToUser("Exception getting menu properties, menus won't work:  " + caught.getMessage());
			}

			@Override
			public void onSuccess(PropertiesPojo result) {
//				demoItem.setTitle(result.getProperty("title", "Unknown"));
//				demoItem.setText(result.getProperty("text", "Unknown"));
				final String target = result.getProperty("target", null);
				final String href = result.getProperty("href", "Unknown");
				demosCommand = new Command() {
					@Override
					public void execute() {
						if (target != null && target.equalsIgnoreCase("_blank")) {
							Window.open(href, "_blank", "");
						}
						else {
							Window.Location.assign(href);
						}
					}
				};
			}
		};
		VpcpLandingService.Util.getInstance().getPropertiesForMenu("demoItem", demo_cb);

		AsyncCallback<PropertiesPojo> awsSupport_cb = new AsyncCallback<PropertiesPojo>() {
			@Override
			public void onFailure(Throwable caught) {
				GWT.log("Exception Menu Properties", caught);
				showMessageToUser("Exception getting menu properties, menus won't work:  " + caught.getMessage());
			}

			@Override
			public void onSuccess(PropertiesPojo result) {
//				awsItem.setTitle(result.getProperty("title", "Unknown"));
//				awsItem.setText(result.getProperty("text", "Unknown"));
				final String target = result.getProperty("target", null);
				final String href = result.getProperty("href", "Unknown");
				gcpSupportCommand = new Command() {
					@Override
					public void execute() {
						if (target != null && target.equalsIgnoreCase("_blank")) {
							Window.open(href, "_blank", "");
						}
						else {
							Window.Location.assign(href);
						}
					}
				};
			}
		};
		VpcpLandingService.Util.getInstance().getPropertiesForMenu("awsSupportItem", awsSupport_cb);

		AsyncCallback<PropertiesPojo> siteSupport_cb = new AsyncCallback<PropertiesPojo>() {
			@Override
			public void onFailure(Throwable caught) {
				GWT.log("Exception Menu Properties", caught);
				showMessageToUser("Exception getting menu properties, menus won't work:  " + caught.getMessage());
			}

			@Override
			public void onSuccess(PropertiesPojo result) {
//				emoryAwsItem.setTitle(result.getProperty("title", "Unknown"));
//				emoryAwsItem.setText(result.getProperty("text", "Unknown"));
				final String target = result.getProperty("target", null);
				final String href = result.getProperty("href", "Unknown");
				siteSupportCommand = new Command() {
					@Override
					public void execute() {
						if (target != null && target.equalsIgnoreCase("_blank")) {
							Window.open(href, "_blank", "");
						}
						else {
							Window.Location.assign(href);
						}
					}
				}; 
			}
		};
		VpcpLandingService.Util.getInstance().getPropertiesForMenu("siteSupportItem", siteSupport_cb);
	}
	
	Command productsCommand = new Command() {
		public void execute() {
			showProductsPopup();
		}
	};
	Command hipaaServicesCommand = new Command() {
		public void execute() {
			showServicesList("List of Services Available for HIPAA Accounts", true);
		}
	};
	Command standardServicesCommand = new Command() {
		public void execute() {
			showServicesList("List of Services Available for Standard Accounts", false);
		}
	};

	private void showServicesList(String header, boolean hipaaOnly) {
		serviceListPanel.clear();
		serviceListPanel.setSpacing(8);
		Button closeButton = new Button("Close List");
		serviceListPanel.add(closeButton);
		closeButton.addStyleName("normalButton");
		closeButton.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				contentPanel.setVisible(true);
				serviceListPanel.setVisible(false);
			}
		});
		
		HorizontalPanel hp = new HorizontalPanel();
		hp.setWidth("100%");
		hp.getElement().getStyle().setBackgroundColor("#3367d6");
		hp.getElement().getStyle().setPadding(2.0, Unit.EM);
		
		serviceListPanel.add(hp);
		HTML headerHTML = new HTML(header);
		headerHTML.getElement().getStyle().setFontSize(2.5, Unit.EM);
		headerHTML.getElement().getStyle().setColor("#fff");
		headerHTML.getElement().getStyle().setFontWeight(FontWeight.BOLD);
		headerHTML.getElement().getStyle().setTextAlign(TextAlign.LEFT);
		headerHTML.getElement().getStyle().setLineHeight(2.0, Unit.EM);
		hp.add(headerHTML);

		StringBuffer sbuf = new StringBuffer();
		if (serviceSummary != null) {
			sbuf.append("<ul>");
			Object[] keys = serviceSummary.getServiceMap().keySet().toArray();
			Arrays.sort(keys);
			int hipaaCnt=0;
			int stdCnt=0;
			int totalSvcCnt=0;
			for (final Object catName : keys) {
				List<AWSServicePojo> services = serviceSummary.getServiceMap().get(catName);
				totalSvcCnt += services.size();
				for (final AWSServicePojo svc : services) {
					String svcName = "Unknown";
					if (svc.getCombinedServiceName() != null) {
						svcName = svc.getCombinedServiceName();
					}
					else if (svc.getAlternateServiceName() != null) {
						svcName = svc.getAlternateServiceName();
					}
					else {
						svcName = svc.getAwsServiceName();
					}
					if (hipaaOnly) {
						if (svc.isAvailableHIPAA() || svc.isAvailableWithCountermeasuresHIPAA()) {
							hipaaCnt++;
							sbuf.append("<h3><li>" + catName + " : " + svcName + "</li></h3>");
						}
					}
					else {
						if (svc.isAvailableStandard() || svc.isAvailableWithCountermeasuresStandard()) {
							stdCnt++;
							sbuf.append("<h3><li>" + catName + " : " + svcName + "</li></h3>");
						}
					}
				}
			}
			GWT.log("Total Services count: " + totalSvcCnt);
			GWT.log("HIPAA count: " + hipaaCnt);
			GWT.log("Standard count: " + stdCnt);
			sbuf.append("</ul>");
			HTML svcList = new HTML(sbuf.toString());
			svcList.getElement().getStyle().setTextAlign(TextAlign.LEFT);
			VerticalPanel vp = new VerticalPanel();
			vp.getElement().getStyle().setPaddingLeft(3.5, Unit.EM);
			
			serviceListPanel.add(vp);
			vp.add(svcList);
		}
		else {
			HTML h = new HTML("<h3>Service list is not available yet.  Try again in a bit.</h3>");
			VerticalPanel vp = new VerticalPanel();
			vp.getElement().getStyle().setPaddingLeft(3.5, Unit.EM);
			
			serviceListPanel.add(vp);
			vp.add(h);
		}
		contentPanel.setVisible(false);
		serviceListPanel.setVisible(true);
	}
	
    PopupPanel productsPopup = new PopupPanel(true);
    boolean productsShowing=false;
    
    @UiField DeckLayoutPanel carouselContainer;

//    @UiHandler ("hamburgerMenu")
//    void hamburgerMenuClicked(ClickEvent e) {
//		productsPopup.hide();
//		contentPanel.setVisible(true);
//		serviceListPanel.setVisible(false);
//		hamburgerPopup = new PopupPanel(true);
//		hamburgerPopup.setWidth("250px");
//		VerticalPanel vp = new VerticalPanel();
//		vp.setSpacing(12);
//		for (int i=0; i<20; i++) {
//			HTML h = new HTML("This is the " + i + "th item in the list");
//			h.getElement().getStyle().setFontSize(15, Unit.PX);
//			vp.add(h);
//		}
//		hamburgerPopup.add(vp);
//		hamburgerPopup.showRelativeTo(hamburgerMenu);
//    }
    
	@UiHandler ("awsConsoleButton")
	void awsConsoleButtonMouseOver(MouseOverEvent e) {
		// get this from the service layer so it can be externalized
		AsyncCallback<String> callback = new AsyncCallback<String>() {
			@Override
			public void onFailure(Throwable caught) {
				GWT.log("Can't get AWS Console info text", caught);
				awsConsoleButton.setTitle("Can't get AWS Console info text");
			}

			@Override
			public void onSuccess(String result) {
				awsConsoleButton.setTitle(result);
			}
		};
		VpcpLandingService.Util.getInstance().getAwsConsoleInfoText(callback);
	}
    
	@UiHandler ("awsConsoleButton")
	void awsConsoleButtonClicked(ClickEvent e) {
		// get this from the service layer so it can be externalized
		AsyncCallback<String> callback = new AsyncCallback<String>() {
			@Override
			public void onFailure(Throwable caught) {
				GWT.log("Exception getting AWS console URL", caught);
				showMessageToUser("Exception getting AWS console URL:  " + caught.getMessage());
			}

			@Override
			public void onSuccess(String result) {
				Window.Location.assign(result);
			}
		};
		leavingPage=true;
		VpcpLandingService.Util.getInstance().getAwsConsoleURL(callback);
	}

	@UiHandler ("emoryConsoleButton")
	void emoryConsoleButtonMouseOver(MouseOverEvent e) {
		// get this from the service layer so it can be externalized
		AsyncCallback<String> callback = new AsyncCallback<String>() {
			@Override
			public void onFailure(Throwable caught) {
				GWT.log("Can't get VPCP Console URL", caught);
				emoryConsoleButton.setTitle("Can't get VPCP Console URL");
			}

			@Override
			public void onSuccess(String result) {
				emoryConsoleButton.setTitle(result);
			}
		};
		VpcpLandingService.Util.getInstance().getEmoryConsoleURL(callback);
	}
	@UiHandler ("emoryConsoleButton")
	void emoryConsoleButtonClicked(ClickEvent e) {
		// get this from the service layer so it can be externalized
		AsyncCallback<String> callback = new AsyncCallback<String>() {
			@Override
			public void onFailure(Throwable caught) {
				GWT.log("Exception getting VPCP console URL", caught);
				showMessageToUser("Exception getting VPCP console URL:  " + caught.getMessage());
			}

			@Override
			public void onSuccess(String result) {
				Window.Location.assign(result);
			}
		};
		leavingPage=true;
		VpcpLandingService.Util.getInstance().getEmoryConsoleURL(callback);
	}

	@UiHandler ("createAccountButton")
	void serviceNowMouseOver(MouseOverEvent e) {
		// get this from the service layer so it can be externalized
		AsyncCallback<String> callback = new AsyncCallback<String>() {
			@Override
			public void onFailure(Throwable caught) {
				GWT.log("Can't get Console URL", caught);
				createAccountButton.setTitle("Can't get Console URL");
			}

			@Override
			public void onSuccess(String result) {
				createAccountButton.setTitle(result);
			}
		};
		VpcpLandingService.Util.getInstance().getCreateAccountURL(callback);
	}
	@UiHandler ("createAccountButton")
	void serviceNowButtonClicked(ClickEvent e) {
		// get this from the service layer so it can be externalized
		AsyncCallback<String> callback = new AsyncCallback<String>() {
			@Override
			public void onFailure(Throwable caught) {
				GWT.log("Exception getting the URL needed to create an account.", caught);
				showMessageToUser("Exception getting the URL needed to create an account:  " + caught.getMessage());
			}

			@Override
			public void onSuccess(String result) {
				Window.Location.assign(result);
			}
		};
		leavingPage=true;
		VpcpLandingService.Util.getInstance().getCreateAccountURL(callback);
	}

	Command esbServiceStatusCommand = new Command() {
		public void execute() {
			AsyncCallback<String> callback = new AsyncCallback<String>() {
				@Override
				public void onFailure(Throwable caught) {
					String msg = "Exception getting ESB service status URL"; 
					GWT.log(msg, caught);
					showMessageToUser(msg + " "	 + caught.getMessage());
				}

				@Override
				public void onSuccess(String result) {
					GWT.log("opening " + result);
					Window.open(result, "_blank", "");
				}
			};
			VpcpLandingService.Util.getInstance().getEsbServiceStatusURL(callback);
		}
	};

	Command monitoringStatusCommand = new Command() {
		public void execute() {
			AsyncCallback<String> callback = new AsyncCallback<String>() {
				@Override
				public void onFailure(Throwable caught) {
					String msg = "Exception getting beta ESB service status URL"; 
					GWT.log(msg, caught);
					showMessageToUser(msg + " "	 + caught.getMessage());
				}

				@Override
				public void onSuccess(String result) {
					GWT.log("opening " + result);
					Window.open(result, "_blank", "");
				}
			};
			VpcpLandingService.Util.getInstance().getMonitoringStatusURL(callback);
		}
	};

	private void registerEvents() {
//		monitoringStatusItem.setScheduledCommand(monitoringStatusCommand);
//		esbServiceStatusItem.setScheduledCommand(esbServiceStatusCommand);
//		productsItem.setScheduledCommand(productsCommand);
//		hipaaServicesItem.setScheduledCommand(hipaaServicesCommand);
//		standardServicesItem.setScheduledCommand(standardServicesCommand);

		Event.sinkEvents(logoElem, Event.ONCLICK);
		Event.setEventListener(logoElem, new EventListener() {
			@Override
			public void onBrowserEvent(Event event) {
				if(Event.ONCLICK == event.getTypeInt()) {
					productsPopup.hide();
					contentPanel.setVisible(true);
					serviceListPanel.setVisible(false);
					startCarouselTimer(5000);
				}
			}
		});

		// serviceOverviewImageElem
	    Event.sinkEvents(serviceOverviewImageElem, Event.ONCLICK);
	    Event.setEventListener(serviceOverviewImageElem, new EventListener() {
	        @Override
	        public void onBrowserEvent(Event event) {
	             if (Event.ONCLICK == event.getTypeInt()) {
	            	 openServiceOverviewWindow();
	             }
	        }
	    });
		// serviceOverviewElem
	    Event.sinkEvents(serviceOverviewElem, Event.ONCLICK);
	    Event.setEventListener(serviceOverviewElem, new EventListener() {
	        @Override
	        public void onBrowserEvent(Event event) {
	             if (Event.ONCLICK == event.getTypeInt()) {
	            	 openServiceOverviewWindow();
	             }
	        }
	    });
		// useCasesImageElem
	    Event.sinkEvents(useCasesImageElem, Event.ONCLICK);
	    Event.setEventListener(useCasesImageElem, new EventListener() {
	        @Override
	        public void onBrowserEvent(Event event) {
	             if (Event.ONCLICK == event.getTypeInt()) {
	            	 openUseCasesWindow();
	             }
	        }
	    });
		// useCasesElem
	    Event.sinkEvents(useCasesElem, Event.ONCLICK);
	    Event.setEventListener(useCasesElem, new EventListener() {
	        @Override
	        public void onBrowserEvent(Event event) {
	             if (Event.ONCLICK == event.getTypeInt()) {
	            	 openUseCasesWindow();
	             }
	        }
	    });
		// gettingStartedImageElem
	    Event.sinkEvents(gettingStartedImageElem, Event.ONCLICK);
	    Event.setEventListener(gettingStartedImageElem, new EventListener() {
	        @Override
	        public void onBrowserEvent(Event event) {
	             if (Event.ONCLICK == event.getTypeInt()) {
	            	 openGettingStartedWindow();
	             }
	        }
	    });
		// gettingStartedElem
	    Event.sinkEvents(gettingStartedElem, Event.ONCLICK);
	    Event.setEventListener(gettingStartedElem, new EventListener() {
	        @Override
	        public void onBrowserEvent(Event event) {
	             if (Event.ONCLICK == event.getTypeInt()) {
	            	 openGettingStartedWindow();
	             }
	        }
	    });
	    
	    // faqImageElem
	    Event.sinkEvents(faqImageElem, Event.ONCLICK);
	    Event.setEventListener(faqImageElem, new EventListener() {
	        @Override
	        public void onBrowserEvent(Event event) {
	             if (Event.ONCLICK == event.getTypeInt()) {
	            	 openFaqWindow();
	             }
	        }
	    });
	    // securityImageElem
	    Event.sinkEvents(securityImageElem, Event.ONCLICK);
	    Event.setEventListener(securityImageElem, new EventListener() {
	        @Override
	        public void onBrowserEvent(Event event) {
	             if (Event.ONCLICK == event.getTypeInt()) {
	            	 openSecurityWindow();
	             }
	        }
	    });
	    // supportImageElem
	    Event.sinkEvents(supportImageElem, Event.ONCLICK);
	    Event.setEventListener(supportImageElem, new EventListener() {
	        @Override
	        public void onBrowserEvent(Event event) {
	             if (Event.ONCLICK == event.getTypeInt()) {
	            	 openSupportWindow();
	             }
	        }
	    });
	    // architectureImageElem
	    Event.sinkEvents(architectureImageElem, Event.ONCLICK);
	    Event.setEventListener(architectureImageElem, new EventListener() {
	        @Override
	        public void onBrowserEvent(Event event) {
	             if (Event.ONCLICK == event.getTypeInt()) {
	            	 openArchitectureWindow();
	             }
	        }
	    });
	    // partnersImageElem
	    Event.sinkEvents(partnersImageElem, Event.ONCLICK);
	    Event.setEventListener(partnersImageElem, new EventListener() {
	        @Override
	        public void onBrowserEvent(Event event) {
	             if (Event.ONCLICK == event.getTypeInt()) {
	            	 openPartnersWindow();
	             }
	        }
	    });
	    // grantsImageElem
	    Event.sinkEvents(grantsImageElem, Event.ONCLICK);
	    Event.setEventListener(grantsImageElem, new EventListener() {
	        @Override
	        public void onBrowserEvent(Event event) {
	             if (Event.ONCLICK == event.getTypeInt()) {
	            	 openGrantsWindow();
	             }
	        }
	    });
	    // trainingImageElem
	    Event.sinkEvents(trainingImageElem, Event.ONCLICK);
	    Event.setEventListener(trainingImageElem, new EventListener() {
	        @Override
	        public void onBrowserEvent(Event event) {
	             if (Event.ONCLICK == event.getTypeInt()) {
	            	 openTrainingWindow();
	             }
	        }
	    });
	    // fundingImageElem
	    Event.sinkEvents(fundingImageElem, Event.ONCLICK);
	    Event.setEventListener(fundingImageElem, new EventListener() {
	        @Override
	        public void onBrowserEvent(Event event) {
	             if (Event.ONCLICK == event.getTypeInt()) {
	            	 openFundingWindow();
	             }
	        }
	    });
	    // resourcesImageElem
	    Event.sinkEvents(resourcesImageElem, Event.ONCLICK);
	    Event.setEventListener(resourcesImageElem, new EventListener() {
	        @Override
	        public void onBrowserEvent(Event event) {
	             if (Event.ONCLICK == event.getTypeInt()) {
	            	 openResourcesWindow();
	             }
	        }
	    });
		
	    Event.sinkEvents(productsPopup.getElement(), Event.ONMOUSEOUT);
	    Event.setEventListener(productsPopup.getElement(), new EventListener() {
	        @Override
	        public void onBrowserEvent(Event event) {
	             if(Event.ONMOUSEOUT == event.getTypeInt()) {
	            	 	productsPopup.hide();
	             }
	        }
	    });
	    
//	    Event.sinkEvents(productsElem, Event.ONCLICK);
//	    Event.setEventListener(productsElem, new EventListener() {
//	        @Override
//	        public void onBrowserEvent(Event event) {
//	             if(Event.ONCLICK == event.getTypeInt()) {
//					showServices();
//	             }
//	        }
//	    });
	}
	
	public DesktopAppShell(String firstName) {
		GWT.log("DESKTOP SHELL:  first name");
		initWidget(uiBinder.createAndBindUi(this));
	}

	@Override
	public void setWidget(IsWidget w) {
	}

	void showProductsPopup() {
		PushButton refreshButton = new PushButton();
		refreshButton.setTitle("Refresh list");
		refreshButton.setWidth("30px");
		refreshButton.setHeight("30px");
		Image img = new Image("images/refresh_icon.png");
		img.setWidth("30px");
		img.setHeight("30px");
		refreshButton.getUpFace().setImage(img);
		refreshButton.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				refreshServiceMap(false);
			}
		});

		productsPopup.clear();
		productsPopup.setAutoHideEnabled(true);
		productsPopup.setWidth("1200px");
		productsPopup.setHeight("800px");
		productsPopup.setAnimationEnabled(true);
		productsPopup.getElement().getStyle().setBackgroundColor("#232f3e");
		productsPopup.addCloseHandler(new CloseHandler<PopupPanel>() {
			@Override
			public void onClose(CloseEvent<PopupPanel> event) {
				productsShowing = false;
			}
		});
		
		ScrollPanel sp = new ScrollPanel();
		sp.getElement().getStyle().setBackgroundColor("#232f3e");
		sp.getElement().getStyle().setBorderColor("black");
		sp.getElement().getStyle().setBorderStyle(BorderStyle.SOLID);
		sp.setHeight("99%");
		sp.setWidth("100%");
		productsPopup.add(sp);
		
		VerticalPanel mainPanel = new VerticalPanel();
		mainPanel.setWidth("100%");
		mainPanel.setHeight("800px");
		mainPanel.getElement().getStyle().setBackgroundColor("#232f3e");
		sp.add(mainPanel);
		mainPanel.add(refreshButton);
		
		VerticalPanel svcStatsVp = new VerticalPanel();
		mainPanel.add(svcStatsVp);
		svcStatsVp.setSpacing(8);
		svcStatsVp.getElement().getStyle().setBackgroundColor("#232f3e");
		svcStatsVp.getElement().getStyle().setBorderColor("black");
		svcStatsVp.setWidth("100%");
		HTML svcStatsHeading = new HTML(siteSpecificServiceName + " Service at a Glance");
		svcStatsHeading.getElement().getStyle().setColor("#ddd");
		svcStatsHeading.getElement().getStyle().setFontSize(20, Unit.PX);
		svcStatsHeading.getElement().getStyle().setFontWeight(FontWeight.BOLD);
		svcStatsVp.add(svcStatsHeading);
		
		if (serviceSummary != null) {
			StringBuffer sbuf = new StringBuffer();
			sbuf.append("<ul>");

			FlexTable svcStatsTable = new FlexTable();
			svcStatsVp.add(svcStatsTable);
			
			// AWS stats in column 1
			HTML awsColumnHeading = new HTML("AWS Service Statistics");
			awsColumnHeading.getElement().getStyle().setColor("#ddd");
			awsColumnHeading.getElement().getStyle().setFontSize(16, Unit.PX);
			awsColumnHeading.getElement().getStyle().setFontWeight(FontWeight.BOLD);
			svcStatsTable.setWidget(0, 0, awsColumnHeading);
			svcStatsTable.getCellFormatter().setHorizontalAlignment(0, 0, HasHorizontalAlignment.ALIGN_CENTER);
			svcStatsTable.getCellFormatter().setVerticalAlignment(0, 0, HasVerticalAlignment.ALIGN_BOTTOM);
			
			for (AWSServiceStatisticPojo stat : serviceSummary.getAwsServiceStatistics()) {
				sbuf.append("<li>" + stat.getStatisticName() + ":  " + stat.getCount() + "</li>");
			}
			
			sbuf.append("</ul>");
			HTML statHtml = new HTML(sbuf.toString());
			statHtml.getElement().getStyle().setColor("orange");
			statHtml.getElement().getStyle().setFontSize(14, Unit.PX);
			statHtml.getElement().getStyle().setFontWeight(FontWeight.BOLD);
			svcStatsTable.setWidget(1, 0, statHtml);
			svcStatsTable.getCellFormatter().setVerticalAlignment(1, 0, HasVerticalAlignment.ALIGN_TOP);

			// site stats in column 2
			sbuf = new StringBuffer();
			sbuf.append("<ul>");
			HTML siteColumnHeading = new HTML("Site Specific Service Statistics");
			siteColumnHeading.getElement().getStyle().setColor("#ddd");
			siteColumnHeading.getElement().getStyle().setFontSize(16, Unit.PX);
			siteColumnHeading.getElement().getStyle().setFontWeight(FontWeight.BOLD);
			svcStatsTable.setWidget(0, 1, siteColumnHeading);
			svcStatsTable.getCellFormatter().setHorizontalAlignment(0, 1, HasHorizontalAlignment.ALIGN_CENTER);
			svcStatsTable.getCellFormatter().setVerticalAlignment(0, 1, HasVerticalAlignment.ALIGN_BOTTOM);

			for (AWSServiceStatisticPojo stat : serviceSummary.getSiteServiceStatistics()) {
				sbuf.append("<li>" + stat.getStatisticName() + ":  " + stat.getCount() + "</li>");
			}
			
			sbuf.append("</ul>");
			HTML statHtml2 = new HTML(sbuf.toString());
			statHtml2.getElement().getStyle().setColor("orange");
			statHtml2.getElement().getStyle().setFontSize(14, Unit.PX);
			statHtml2.getElement().getStyle().setFontWeight(FontWeight.BOLD);
			svcStatsTable.setWidget(1, 1, statHtml2);
			svcStatsTable.getCellFormatter().setVerticalAlignment(1, 1, HasVerticalAlignment.ALIGN_TOP);
		}
		else {
			HTML h = new HTML("Service Statistics are not available yet.  Try again in a bit.");
			h.getElement().getStyle().setColor("orange");
			h.getElement().getStyle().setFontSize(16, Unit.PX);
			h.getElement().getStyle().setFontWeight(FontWeight.BOLD);
			svcStatsVp.add(h);
			productsPopup.showRelativeTo(linksPanel);
			return;
		}

		// add a search panel to the mainPanel above the catSvcAssessmentHP
		Grid searchGrid = new Grid(2,3);
		searchGrid.getElement().getStyle().setBackgroundColor("#232f3e");
		mainPanel.add(searchGrid);
		HTML searchIntro = new HTML("<b>Search for a specific service</b>");
		searchIntro.getElement().getStyle().setBackgroundColor("#232f3e");
		searchIntro.getElement().getStyle().setColor("#ddd");
		searchIntro.getElement().getStyle().setFontSize(16, Unit.PX);
		searchIntro.getElement().getStyle().setFontWeight(FontWeight.BOLD);
		searchGrid.setWidget(0, 0, searchIntro);

		final TextBox searchTB = new TextBox();
		searchGrid.setWidget(1, 0, searchTB);
		searchTB.setText("");
		searchTB.getElement().setPropertyString("placeholder", "enter all or part of the service name");
		searchTB.addStyleName("field");
		searchTB.addStyleName("glowing-border");
		
		Button searchButton = new Button("Search");
		searchGrid.setWidget(1, 2, searchButton);
		searchButton.addStyleName("normalButton");
		searchButton.addStyleName("glowing-border");
		
		HorizontalPanel catSvcAssessmentHP = new HorizontalPanel();
		catSvcAssessmentHP.getElement().getStyle().setBackgroundColor("#232f3e");
		catSvcAssessmentHP.setHeight("100%");
		catSvcAssessmentHP.setSpacing(12);
		mainPanel.add(catSvcAssessmentHP);
		
		Object[] categories = serviceSummary.getServiceMap().keySet().toArray();
		Arrays.sort(categories);

		final VerticalPanel categoryVp = new VerticalPanel();
		categoryVp.getElement().getStyle().setBackgroundColor("#232f3e");
		categoryVp.setHeight("100%");
		categoryVp.setWidth("325px");
		categoryVp.setSpacing(8);
		catSvcAssessmentHP.add(categoryVp);
		
		HTML catHeading = new HTML("Browse Service Categories");
		catHeading.getElement().getStyle().setBackgroundColor("#232f3e");
		catHeading.getElement().getStyle().setColor("#ddd");
		catHeading.getElement().getStyle().setFontSize(20, Unit.PX);
		catHeading.getElement().getStyle().setFontWeight(FontWeight.BOLD);
		categoryVp.add(catHeading);

		final Grid categoryGrid = new Grid(categories.length, 1);
		categoryGrid.getElement().getStyle().setBackgroundColor("#232f3e");
		categoryVp.add(categoryGrid);

		final VerticalPanel servicesVp = new VerticalPanel();
		servicesVp.ensureDebugId("servicesVp");
		servicesVp.getElement().getStyle().setBackgroundColor("#232f3e");
		servicesVp.setWidth("400px");
		servicesVp.setSpacing(8);
		catSvcAssessmentHP.add(servicesVp);
		
		final VerticalPanel assessmentVp = new VerticalPanel();
		assessmentVp.getElement().getStyle().setBackgroundColor("#232f3e");
		assessmentVp.setWidth("425px");
		assessmentVp.setSpacing(8);
		catSvcAssessmentHP.add(assessmentVp);
		
		// allow enter key on search field to trigger search also
		// get service that have a "fuzzy" match to the info typed in the search text box
		searchTB.addKeyDownHandler(new KeyDownHandler() {
			@Override
			public void onKeyDown(KeyDownEvent event) {
				if (event.getNativeKeyCode() == KeyCodes.KEY_ENTER) {
					doServiceSearch(searchTB.getText(), servicesVp, assessmentVp);
				}
			}
		});
		searchButton.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				doServiceSearch(searchTB.getText(), servicesVp, assessmentVp);
			}
		});

		Object[] keys = serviceSummary.getServiceMap().keySet().toArray();
		Arrays.sort(keys);
		int categoryRowCnt = 0;
		for (final Object catName : keys) {
			Anchor categoryAnchor = new Anchor((String)catName);
			categoryAnchor.addStyleName("categoryAnchor");
			categoryAnchor.getElement().getStyle().setBackgroundColor("#232f3e");
			categoryAnchor.getElement().getStyle().setColor("#ddd");
			categoryAnchor.getElement().getStyle().setFontSize(16, Unit.PX);
			categoryAnchor.getElement().getStyle().setFontWeight(FontWeight.BOLD);
			categoryAnchor.getElement().getStyle().setPaddingLeft(10, Unit.PX);
			categoryAnchor.addClickHandler(new ClickHandler() {
				@Override
				public void onClick(ClickEvent event) {
					List<AWSServicePojo> services = serviceSummary.getServiceMap().get(catName);
					assessmentVp.clear();
					servicesVp.clear();
					
					HTML svcCatHeading = new HTML((String)catName);
					svcCatHeading.getElement().getStyle().setBackgroundColor("#232f3e");
					svcCatHeading.getElement().getStyle().setColor("#ddd");
					svcCatHeading.getElement().getStyle().setFontSize(20, Unit.PX);
					svcCatHeading.getElement().getStyle().setFontWeight(FontWeight.BOLD);
					servicesVp.add(svcCatHeading);

					for (final AWSServicePojo svc : services) {
						addServiceToServicesPanel(servicesVp, assessmentVp, svc);
					}
				}
			});
			categoryGrid.setWidget(categoryRowCnt, 0, categoryAnchor);
			categoryRowCnt++;
		}

		productsPopup.showRelativeTo(linksPanel);
	}

	// called when search is clicked or enter key is pressed on search
	void doServiceSearch(String searchString, final VerticalPanel servicesVp, final VerticalPanel assessmentVp) {
		showPleaseWaitDialog("Retrieving services from the AWS Account Service...");
		AWSServiceQueryFilterPojo filter;
		filter = new AWSServiceQueryFilterPojo();
		filter.setAwsServiceName(searchString);
		filter.setFuzzyFilter(true);
		
		AsyncCallback<AWSServiceQueryResultPojo> callback = new AsyncCallback<AWSServiceQueryResultPojo>() {
			@Override
			public void onFailure(Throwable caught) {
                hidePleaseWaitDialog();
				log.log(Level.SEVERE, "Exception Retrieving Services", caught);
				showMessageToUser("There was an exception on the " +
						"server retrieving the list of Services.  " +
						"<p>Message from server is: " + caught.getMessage() + "</p>");
			}

			@Override
			public void onSuccess(AWSServiceQueryResultPojo result) {
				GWT.log("Got " + result.getResults().size() + " Services for " + result.getFilterUsed());
				servicesVp.clear();
				assessmentVp.clear();
				if (result == null || result.getResults().size() == 0) {
					// no services found
					HTML notFoundHTML = new HTML("-- No Services Found --");
					notFoundHTML.getElement().getStyle().setFontSize(16, Unit.PX);
					notFoundHTML.getElement().getStyle().setFontWeight(FontWeight.BOLD);
					notFoundHTML.getElement().getStyle().setColor("#fff");
					servicesVp.add(notFoundHTML);
				}
				for (final AWSServicePojo svc : result.getResults()) {
					addServiceToServicesPanel(servicesVp, assessmentVp, svc);
				}
                hidePleaseWaitDialog();
			}
		};

		GWT.log("refreshing Services list...");
		VpcpLandingService.Util.getInstance().getServicesForFilter(filter, callback);
	}
	// this method will be used by the normal functionality and the search functionality
	void addServiceToServicesPanel(VerticalPanel servicesVp, final VerticalPanel assessmentVp, final AWSServicePojo svc) {
		GWT.log("Adding service: " + svc.getAwsServiceName());
		
		Grid svcGrid = new Grid(4, 2);
		svcGrid.getElement().getStyle().setBackgroundColor("#232f3e");
		servicesVp.add(svcGrid);

		// the service
		final Anchor svcAnchor = new Anchor();
		svcGrid.setWidget(0, 0, svcAnchor);
		if (svc.getCombinedServiceName() != null && 
			svc.getCombinedServiceName().length() > 0) {
			svcAnchor.setText(svc.getCombinedServiceName());
		}
		else if (svc.getAlternateServiceName() != null && 
				svc.getAlternateServiceName().length() > 0 ) {
			svcAnchor.setText(svc.getAlternateServiceName());
		}
		else {
			svcAnchor.setText(svc.getAwsServiceName());
		}
		
		svcAnchor.addStyleName("productAnchor");
		svcAnchor.getElement().getStyle().setFontSize(16, Unit.PX);
		svcAnchor.getElement().getStyle().setFontWeight(FontWeight.BOLD);
		svcAnchor.getElement().getStyle().setColor("#fff");
		svcAnchor.setTitle("STATUS: " + svc.getSiteStatus()); 
		svcAnchor.setHref(svc.getAwsLandingPageUrl());
		svcAnchor.setTarget("_blank");

		// get service assessment info on mouseover
		svcAnchor.addMouseOverHandler(new MouseOverHandler() {
			@Override
			public void onMouseOver(MouseOverEvent event) {
				showPleaseWaitDialog("Retrieving assessment information...");
				assessmentVp.clear();
				
				HTML assessmentHeading = new HTML("Assessment of the " + svcAnchor.getText() + " service." );
				assessmentHeading.getElement().getStyle().setBackgroundColor("#232f3e");
				assessmentHeading.getElement().getStyle().setColor("#ddd");
				assessmentHeading.getElement().getStyle().setFontSize(16, Unit.PX);
				assessmentHeading.getElement().getStyle().setFontWeight(FontWeight.BOLD);
				assessmentHeading.setTitle("Log in to the VPCP Console to see the full assessment.");
				assessmentVp.add(assessmentHeading);
				assessmentVp.add(new HTML("</br>"));

				// add service assessment info if it exists
				ServiceSecurityAssessmentQueryFilterPojo filter = new ServiceSecurityAssessmentQueryFilterPojo();
				filter.setServiceId(svc.getServiceId());
				AsyncCallback<ServiceSecurityAssessmentQueryResultPojo> assessmentCb = new AsyncCallback<ServiceSecurityAssessmentQueryResultPojo>() {
					@Override
					public void onFailure(Throwable caught) {
						HTML assessmentHtml = new HTML("Error retrieving assessment information.");
						assessmentHtml.getElement().getStyle().setBackgroundColor("#232f3e");
						assessmentHtml.getElement().getStyle().setColor("#ddd");
						assessmentHtml.getElement().getStyle().setFontSize(14, Unit.PX);
						assessmentVp.add(assessmentHtml);
						hidePleaseWaitDialog();
					}

					@Override
					public void onSuccess(ServiceSecurityAssessmentQueryResultPojo result) {
						if (result.getResults().size() > 0) {
							// get all relevant assessment info for the service
							for (ServiceSecurityAssessmentPojo assessment : result.getResults()) {
								StringBuffer sbuf = new StringBuffer();
								sbuf.append("<b>Assessment status:</b>  " + assessment.getStatus());
								sbuf.append("<ol>");
								for (SecurityRiskPojo sr : assessment.getSecurityRisks()) {
									sbuf.append("<li>" + sr.getDescription() + "</li>");
								}
								sbuf.append("</ol>");
								HTML assessmentHtml = new HTML(sbuf.toString());
								assessmentHtml.getElement().getStyle().setColor("#232f3e");
								assessmentHtml.getElement().getStyle().setColor("#ddd");
								assessmentHtml.getElement().getStyle().setFontSize(14, Unit.PX);
								assessmentVp.add(assessmentHtml);
							}
							hidePleaseWaitDialog();
						}
						else {
							StringBuffer sbuf = new StringBuffer();
							sbuf.append("<b>No Security Assessment Yet</b>");
							HTML assessmentHtml = new HTML(sbuf.toString());
							assessmentHtml.getElement().getStyle().setBackgroundColor("#232f3e");
							assessmentHtml.getElement().getStyle().setColor("#ddd");
							assessmentHtml.getElement().getStyle().setFontSize(14, Unit.PX);
							assessmentVp.add(assessmentHtml);
							hidePleaseWaitDialog();
						}
					}
				};
				VpcpLandingService.Util.getInstance().getSecurityAssessmentsForFilter(filter, assessmentCb);
			}
		});
		
		// emory status
		HTML svcStatus = new HTML("STATUS: " + svc.getSiteStatus());
		svcStatus.addStyleName("productDescription");
		svcStatus.getElement().getStyle().setColor("orange");
		svcStatus.getElement().getStyle().setFontSize(14, Unit.PX);
		svcStatus.getElement().getStyle().setFontWeight(FontWeight.BOLD);
		svcGrid.setWidget(1, 0, svcStatus);
		
		if (!svc.isBlocked() && !svc.isBlockedPendingReview()) {
			Image img = new Image("images/green-checkbox-icon-15.jpg");
			img.getElement().getStyle().setBackgroundColor("#232f3e");
			img.setWidth("16px");
			img.setHeight("16px");
			svcGrid.setWidget(1, 1, img);
		}
		else {
			// red circle with line NOT hipaa eligible
			Image img = new Image("images/red-circle-white-x.png");
			img.getElement().getStyle().setBackgroundColor("#232f3e");
			img.setWidth("16px");
			img.setHeight("16px");
			svcGrid.setWidget(1, 1, img);
		}

		// emory hipaa eligibility
		HTML svcHipaaStatus = new HTML(siteName + " HIPAA Eligibility: " + (svc.isSiteHipaaEligible() ? "Eligible" : "Not Eligible"));
		svcHipaaStatus.addStyleName("productDescription");
		svcHipaaStatus.getElement().getStyle().setColor("orange");
		svcHipaaStatus.getElement().getStyle().setFontSize(14, Unit.PX);
		svcHipaaStatus.getElement().getStyle().setFontWeight(FontWeight.BOLD);
		svcGrid.setWidget(2, 0, svcHipaaStatus);

		if (svc.isSiteHipaaEligible()) {
			Image img = new Image("images/green-checkbox-icon-15.jpg");
			img.getElement().getStyle().setBackgroundColor("#232f3e");
			img.setWidth("16px");
			img.setHeight("16px");
			img.setTitle("This service IS HIPAA eligible according to " + siteName + "'s HIPAA policy");
			svcGrid.setWidget(2, 1, img);
		}
		else {
			// red circle with line NOT hipaa eligible
			Image img = new Image("images/red-circle-white-x.png");
			img.getElement().getStyle().setBackgroundColor("#232f3e");
			img.setWidth("16px");
			img.setHeight("16px");
			img.setTitle("This service IS NOT HIPAA eligible according to " + siteName + "'s HIPAA policy");
			svcGrid.setWidget(2, 1, img);
		}
		
		// service description
		HTML svcDesc = new HTML(svc.getDescription());
		svcDesc.addStyleName("productDescription");
		svcDesc.getElement().getStyle().setColor("#ddd");
		svcDesc.getElement().getStyle().setFontSize(14, Unit.PX);
		svcGrid.setWidget(3, 0, svcDesc);
		
		servicesVp.add(new HTML("</br>"));
	}
	
	void showServices() {
		if (!refreshingServices) {
			if (!productsShowing) {
				productsShowing = true;
			}
			else {
				productsShowing = false;
				productsPopup.hide();
				return;
			}
		
			this.showPleaseWaitDialog("Retrieving services from the AWS Account Service...");
			if (serviceSummary == null || 
				serviceSummary.getServiceMap() == null || 
				serviceSummary.getServiceMap().size() == 0) {
				
				this.refreshServiceMap(true);
			}
			else {
				this.showProductsPopup();
				hidePleaseWaitDialog();
			}
		}
		else {
			showMessageToUser("Product information is being refreshed, please try again in a few seconds.");
		}
	}

	private void refreshServiceMap(final boolean showPopup) {
		AsyncCallback<AWSServiceSummaryPojo> callback = new AsyncCallback<AWSServiceSummaryPojo>() {
			@Override
			public void onFailure(Throwable caught) {
				refreshingServices = false;
				hidePleaseWaitDialog();
				if (!leavingPage) {
					GWT.log("problem getting services..." + caught.getMessage());
					showMessageToUser("Unable to display product information at this "
							+ "time.  Please try again later.  "
							+ "<p>Message from server is: " + caught.getMessage() + "</p>");
				}
			}

			@Override
			public void onSuccess(AWSServiceSummaryPojo result) {
				serviceSummary = result;
				GWT.log("[refreshServiceMap] got " + result.getServiceMap().size() + " categories of services back.");
				refreshingServices = false;
				if (serviceSummary.getServiceMap() == null || serviceSummary.getServiceMap().size() == 0) {
					// there's an issue
					showMessageToUser("Unable to display product information at this time.  Please try again later.");
				}
				else {
					if (showPopup) {
						showProductsPopup();
					}
				}
				hidePleaseWaitDialog();
			}
		};
		if (!refreshingServices) {
			refreshingServices = true;
			VpcpLandingService.Util.getInstance().getAWSServiceMap(callback);
		}
	}

	public void showMessageToUser(String message) {
		VpcpAlert.alert("Alert", message);
	}

//	private void openWelcomeWindow() {
//		AsyncCallback<String> callback = new AsyncCallback<String>() {
//			@Override
//			public void onFailure(Throwable caught) {
//				String msg = "Exception getting Welcome URL"; 
//				GWT.log(msg, caught);
//				showMessageToUser(msg + " "	 + caught.getMessage());
//			}
//
//			@Override
//			public void onSuccess(String result) {
//				GWT.log("opening " + result);
//				Window.open(result, "_blank", "");
//			}
//		};
//		VpcpLandingService.Util.getInstance().getWelcomeURL(callback);
//	}
	private void openServiceOverviewWindow() {
		AsyncCallback<String> callback = new AsyncCallback<String>() {
			@Override
			public void onFailure(Throwable caught) {
				String msg = "Exception getting Service Overview URL"; 
				GWT.log(msg, caught);
				showMessageToUser(msg + " "	 + caught.getMessage());
			}

			@Override
			public void onSuccess(String result) {
				GWT.log("opening " + result);
				Window.open(result, "_self", "");
			}
		};
		VpcpLandingService.Util.getInstance().getServiceOverviewURL(callback);
	}
	private void openUseCasesWindow() {
		AsyncCallback<String> callback = new AsyncCallback<String>() {
			@Override
			public void onFailure(Throwable caught) {
				String msg = "Exception getting Use Cases URL"; 
				GWT.log(msg, caught);
				showMessageToUser(msg + " "	 + caught.getMessage());
			}

			@Override
			public void onSuccess(String result) {
				GWT.log("opening " + result);
				Window.open(result, "_self", "");
			}
		};
		VpcpLandingService.Util.getInstance().getUseCasesURL(callback);
	}
	private void openGettingStartedWindow() {
		AsyncCallback<String> callback = new AsyncCallback<String>() {
			@Override
			public void onFailure(Throwable caught) {
				String msg = "Exception getting Getting Started URL"; 
				GWT.log(msg, caught);
				showMessageToUser(msg + " "	 + caught.getMessage());
			}

			@Override
			public void onSuccess(String result) {
				GWT.log("opening " + result);
				Window.open(result, "_self", "");
			}
		};
		VpcpLandingService.Util.getInstance().getGettingStartedURL(callback);
	}
	private void openFaqWindow() {
		AsyncCallback<String> callback = new AsyncCallback<String>() {
			@Override
			public void onFailure(Throwable caught) {
				String msg = "Exception getting FAQ URL"; 
				GWT.log(msg, caught);
				showMessageToUser(msg + " "	 + caught.getMessage());
			}

			@Override
			public void onSuccess(String result) {
				GWT.log("opening " + result);
				Window.open(result, "_self", "");
			}
		};
		VpcpLandingService.Util.getInstance().getFaqURL(callback);
	}
	private void openSecurityWindow() {
		AsyncCallback<String> callback = new AsyncCallback<String>() {
			@Override
			public void onFailure(Throwable caught) {
				String msg = "Exception getting Security URL"; 
				GWT.log(msg, caught);
				showMessageToUser(msg + " "	 + caught.getMessage());
			}

			@Override
			public void onSuccess(String result) {
				GWT.log("opening " + result);
				Window.open(result, "_self", "");
			}
		};
		VpcpLandingService.Util.getInstance().getSecurityURL(callback);
	}
	private void openSupportWindow() {
		AsyncCallback<String> callback = new AsyncCallback<String>() {
			@Override
			public void onFailure(Throwable caught) {
				String msg = "Exception getting Support URL"; 
				GWT.log(msg, caught);
				showMessageToUser(msg + " "	 + caught.getMessage());
			}

			@Override
			public void onSuccess(String result) {
				GWT.log("opening " + result);
				Window.open(result, "_self", "");
			}
		};
		VpcpLandingService.Util.getInstance().getSupportURL(callback);
	}
	private void openArchitectureWindow() {
		AsyncCallback<String> callback = new AsyncCallback<String>() {
			@Override
			public void onFailure(Throwable caught) {
				String msg = "Exception getting Architecture URL"; 
				GWT.log(msg, caught);
				showMessageToUser(msg + " "	 + caught.getMessage());
			}

			@Override
			public void onSuccess(String result) {
				GWT.log("opening " + result);
				Window.open(result, "_self", "");
			}
		};
		VpcpLandingService.Util.getInstance().getArchitectureURL(callback);
	}
	private void openPartnersWindow() {
		AsyncCallback<String> callback = new AsyncCallback<String>() {
			@Override
			public void onFailure(Throwable caught) {
				String msg = "Exception getting Partners URL"; 
				GWT.log(msg, caught);
				showMessageToUser(msg + " "	 + caught.getMessage());
			}

			@Override
			public void onSuccess(String result) {
				GWT.log("opening " + result);
				Window.open(result, "_self", "");
			}
		};
		VpcpLandingService.Util.getInstance().getPartnersURL(callback);
	}
	private void openGrantsWindow() {
		AsyncCallback<String> callback = new AsyncCallback<String>() {
			@Override
			public void onFailure(Throwable caught) {
				String msg = "Exception getting Grants URL"; 
				GWT.log(msg, caught);
				showMessageToUser(msg + " "	 + caught.getMessage());
			}

			@Override
			public void onSuccess(String result) {
				GWT.log("opening " + result);
				Window.open(result, "_self", "");
			}
		};
		VpcpLandingService.Util.getInstance().getGrantsURL(callback);
	}
	private void openTrainingWindow() {
		AsyncCallback<String> callback = new AsyncCallback<String>() {
			@Override
			public void onFailure(Throwable caught) {
				String msg = "Exception getting Training URL"; 
				GWT.log(msg, caught);
				showMessageToUser(msg + " "	 + caught.getMessage());
			}

			@Override
			public void onSuccess(String result) {
				GWT.log("opening " + result);
				Window.open(result, "_self", "");
			}
		};
		VpcpLandingService.Util.getInstance().getTrainingURL(callback);
	}
	private void openFundingWindow() {
		AsyncCallback<String> callback = new AsyncCallback<String>() {
			@Override
			public void onFailure(Throwable caught) {
				String msg = "Exception getting Funding URL"; 
				GWT.log(msg, caught);
				showMessageToUser(msg + " "	 + caught.getMessage());
			}

			@Override
			public void onSuccess(String result) {
				GWT.log("opening " + result);
				Window.open(result, "_self", "");
			}
		};
		VpcpLandingService.Util.getInstance().getFundingURL(callback);
	}
	private void openResourcesWindow() {
		AsyncCallback<String> callback = new AsyncCallback<String>() {
			@Override
			public void onFailure(Throwable caught) {
				String msg = "Exception getting Resources URL"; 
				GWT.log(msg, caught);
				showMessageToUser(msg + " "	 + caught.getMessage());
			}

			@Override
			public void onSuccess(String result) {
				GWT.log("opening " + result);
				Window.open(result, "_self", "");
			}
		};
		VpcpLandingService.Util.getInstance().getResourcesURL(callback);
	}
	@Override
	public void initializeAwsServiceMap() {
		GWT.log("Desktop shell...initializing AWS Service map");

		AsyncCallback<AWSServiceSummaryPojo> callback = new AsyncCallback<AWSServiceSummaryPojo>() {
			@Override
			public void onFailure(Throwable caught) {
				refreshingServices = false;
				if (!leavingPage) {
					GWT.log("problem getting services..." + caught.getMessage());
					showMessageToUser("Unable to display product information at this "
							+ "time.  Please try again later.  "
							+ "<p>Message from server is: " + caught.getMessage() + "</p>");
				}
			}

			@Override
			public void onSuccess(AWSServiceSummaryPojo result) {
				serviceSummary = result;
				GWT.log("[initializeAwsServiceMap] got " + result.getServiceMap().size() + " categories of services back.");
				refreshingServices = false;
			}
		};
		if (!refreshingServices) {
			refreshingServices = true;
			VpcpLandingService.Util.getInstance().getAWSServiceMap(callback);
		}
	}

	public void showPleaseWaitDialog(String pleaseWaitHTML) {
		if (pleaseWaitDialog == null) {
			pleaseWaitDialog = new PopupPanel(false);
		}
		else {
			pleaseWaitDialog.clear();
		}
		VerticalPanel vp = new VerticalPanel();
		vp.getElement().getStyle().setBackgroundColor("#f1f1f1");
		Image img = new Image();
		img.setUrl("images/ajax-loader.gif");
		vp.add(img);
		HTML h = new HTML(pleaseWaitHTML);
		vp.add(h);
		vp.setCellHorizontalAlignment(img, HasHorizontalAlignment.ALIGN_CENTER);
		vp.setCellHorizontalAlignment(h, HasHorizontalAlignment.ALIGN_CENTER);
		pleaseWaitDialog.setWidget(vp);
		pleaseWaitDialog.center();
		pleaseWaitDialog.show();
	}

	public void hidePleaseWaitDialog() {
		if (pleaseWaitDialog != null) {
			pleaseWaitDialog.hide();
		}
	}
	@Override
	public void setLeavingPage(boolean leavingPage) {
		this.leavingPage = leavingPage;
	}

	@Override
	public PropertiesPojo getSiteSpecificProperties() {
		return siteSpecificProperties;
	}

	@Override
	public void setSiteSpecificProperties(PropertiesPojo properties) {
		siteSpecificProperties = properties;
	}

	@Override
	public String getSiteSpecificServiceName() {
		return this.siteSpecificServiceName;
	}

	@Override
	public String getSiteName() {
		return this.siteName;
	}
}
