package edu.emory.oit.vpcplanding.client.desktop;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Widget;

import edu.emory.oit.vpcplanding.client.presenter.ViewImplBase;
import edu.emory.oit.vpcplanding.client.presenter.landing.MainLandingView;
import edu.emory.oit.vpcplanding.shared.UserAccountPojo;

public class DesktopMainLanding extends ViewImplBase implements MainLandingView {
	Presenter presenter;
	UserAccountPojo userLoggedIn;

	private static DesktopMainLandingUiBinder uiBinder = GWT.create(DesktopMainLandingUiBinder.class);

	interface DesktopMainLandingUiBinder extends UiBinder<Widget, DesktopMainLanding> {
	}

	public DesktopMainLanding() {
		initWidget(uiBinder.createAndBindUi(this));
	}

	@UiField
	Button button;

	public DesktopMainLanding(String firstName) {
		initWidget(uiBinder.createAndBindUi(this));
		button.setText(firstName);
	}

	@UiHandler("button")
	void onClick(ClickEvent e) {
		Window.alert("Hello!");
	}

	public void setText(String text) {
		button.setText(text);
	}

	public String getText() {
		return button.getText();
	}

	@Override
	public void setInitialFocus() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Widget getStatusMessageSource() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void applyEmoryAWSAdminMask() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void applyEmoryAWSAuditorMask() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setUserLoggedIn(UserAccountPojo user) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setPresenter(Presenter presenter) {
		this.presenter = presenter;
	}

	@Override
	public void setReleaseInfo(String releaseInfoHTML) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void hidePleaseWaitPanel() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void showPleaseWaitPanel() {
		// TODO Auto-generated method stub
		
	}

}
