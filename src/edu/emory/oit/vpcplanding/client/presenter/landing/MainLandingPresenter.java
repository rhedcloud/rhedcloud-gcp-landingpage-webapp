package edu.emory.oit.vpcplanding.client.presenter.landing;

import java.util.logging.Logger;

import com.google.gwt.user.client.ui.Widget;
import com.google.web.bindery.event.shared.EventBus;

import edu.emory.oit.vpcplanding.client.ClientFactory;

public class MainLandingPresenter implements MainLandingView.Presenter {
	private static final Logger log = Logger.getLogger(MainLandingPresenter.class.getName());
	/**
	 * The delay in milliseconds between calls to refresh the account list.
	 */
	//	  private static final int REFRESH_DELAY = 5000;
	private static final int SESSION_REFRESH_DELAY = 900000;	// 15 minutes

	private final ClientFactory clientFactory;

	private EventBus eventBus;
	
	public MainLandingPresenter(ClientFactory clientFactory, MainLandingPlace place) {
		this.clientFactory = clientFactory;
		getView().setPresenter(this);
	}

	private MainLandingView getView() {
		return clientFactory.getMainLandingView();
	}

	@Override
	public String mayStop() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void start(EventBus eventBus) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void stop() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setInitialFocus() {
		getView().setInitialFocus();
	}

	@Override
	public Widget asWidget() {
		return getView().asWidget();
	}

	@Override
	public EventBus getEventBus() {
		return this.eventBus;
	}

	@Override
	public ClientFactory getClientFactory() {
		return clientFactory;
	}

	public void setEventBus(EventBus eventBus) {
		this.eventBus = eventBus;
	}

}
