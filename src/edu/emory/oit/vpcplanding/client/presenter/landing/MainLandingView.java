package edu.emory.oit.vpcplanding.client.presenter.landing;

import com.google.gwt.user.client.ui.IsWidget;
import com.google.web.bindery.event.shared.EventBus;

import edu.emory.oit.vpcplanding.client.ClientFactory;
import edu.emory.oit.vpcplanding.client.presenter.View;
import edu.emory.oit.vpcplanding.ui.client.PresentsWidgets;

public interface MainLandingView extends IsWidget, View {
	/**
	 * The presenter for this view.
	 */
	public interface Presenter extends PresentsWidgets {
		/**
		 * Select a caseRecord.
		 * 
		 * @param selected the selected caseRecord
		 */
		public EventBus getEventBus();
		public ClientFactory getClientFactory();
	}

	/**
	 * Sets the new presenter, and calls {@link Presenter#stop()} on the previous
	 * one.
	 */
	void setPresenter(Presenter presenter);

	void setReleaseInfo(String releaseInfoHTML);
	void hidePleaseWaitPanel();
	void showPleaseWaitPanel();
}
