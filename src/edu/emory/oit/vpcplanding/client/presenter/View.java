package edu.emory.oit.vpcplanding.client.presenter;

import com.google.gwt.user.client.ui.IsWidget;
import com.google.gwt.user.client.ui.Widget;

import edu.emory.oit.vpcplanding.client.AppShell;
import edu.emory.oit.vpcplanding.shared.UserAccountPojo;

public interface View extends IsWidget {
	void showMessageToUser(String message);
	void showPleaseWaitDialog();
	void hidePleaseWaitDialog();
	void setInitialFocus();
//	void initPage();
	public void showStatus(Widget source, String message);
	public Widget getStatusMessageSource();
//	public void applyAuthorizationMask();
	public void applyEmoryAWSAdminMask();
	public void applyEmoryAWSAuditorMask();
	public void setUserLoggedIn(UserAccountPojo user);
	public void setAppShell(AppShell appShell);
	public AppShell getAppShell();
}
