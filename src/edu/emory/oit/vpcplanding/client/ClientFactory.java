package edu.emory.oit.vpcplanding.client;

import com.google.gwt.place.shared.PlaceController;
import com.google.web.bindery.event.shared.EventBus;

import edu.emory.oit.vpcplanding.client.presenter.landing.MainLandingView;

public interface ClientFactory {
	/**
	 * Create the App.
	 * 
	 * @return a new instance of the {@link AppBootstrapper}
	 */
	AppBootstrapper getApp();

	/**
	 * Get the {@link EventBus}
	 * 
	 * @return the event bus used through the app
	 */
	EventBus getEventBus();

	/**
	 * Get the {@link PlaceController}.
	 * 
	 * @return the place controller
	 */
	PlaceController getPlaceController();

	/**
	 * Get the UI shell.
	 * 
	 * @return the shell
	 */
	AppShell getShell();

	MainLandingView getMainLandingView();
}
