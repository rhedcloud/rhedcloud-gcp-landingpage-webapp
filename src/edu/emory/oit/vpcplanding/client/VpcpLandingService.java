package edu.emory.oit.vpcplanding.client;

import java.util.List;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

import edu.emory.oit.vpcplanding.shared.AWSServiceQueryFilterPojo;
import edu.emory.oit.vpcplanding.shared.AWSServiceQueryResultPojo;
import edu.emory.oit.vpcplanding.shared.AWSServiceSummaryPojo;
import edu.emory.oit.vpcplanding.shared.PropertiesPojo;
import edu.emory.oit.vpcplanding.shared.ReleaseInfo;
import edu.emory.oit.vpcplanding.shared.RpcException;
import edu.emory.oit.vpcplanding.shared.ServiceSecurityAssessmentQueryFilterPojo;
import edu.emory.oit.vpcplanding.shared.ServiceSecurityAssessmentQueryResultPojo;
import edu.emory.oit.vpcplanding.shared.UserAccountPojo;

/**
 * The client-side stub for the RPC service.
 */
@RemoteServiceRelativePath("vpcpLanding")
public interface VpcpLandingService extends RemoteService {
	/**
	 * Utility class for simplifying access to the instance of async service.
	 */
	public static class Util {
		private static VpcpLandingServiceAsync instance;
		public static VpcpLandingServiceAsync getInstance(){
			if (instance == null) {
				instance = GWT.create(VpcpLandingService.class);
			}
			return instance;
		}
	}

	String getLoginURL() throws RpcException;
	String getEsbServiceStatusURL() throws RpcException;
	String getServiceOverviewURL() throws RpcException;
	String getMonitoringStatusURL() throws RpcException;

	AWSServiceSummaryPojo getAWSServiceMap() throws RpcException;
	AWSServiceQueryResultPojo getServicesForFilter(AWSServiceQueryFilterPojo filter) throws RpcException;
	String getAccountSeriesText() throws RpcException;
	String getAwsConsoleInfoText() throws RpcException;
	String getAwsConsoleURL() throws RpcException;
	String getEmoryConsoleURL() throws RpcException;
	String getCreateAccountURL() throws RpcException;
	// ReleaseInfo
	ReleaseInfo getReleaseInfo() throws RpcException;
	UserAccountPojo getUserLoggedIn() throws RpcException;
	String getUseCasesURL();
	String getGettingStartedURL();
	String getArchitectureURL();
	String getFaqURL();
	String getSecurityURL();
	String getSupportURL();
	String getPartnersURL();
	String getGrantsURL();
	String getTrainingURL();
	String getFundingURL();
	String getResourcesURL();
	String getWelcomeURL();
	
	ServiceSecurityAssessmentQueryResultPojo getSecurityAssessmentsForFilter(ServiceSecurityAssessmentQueryFilterPojo filter) throws RpcException;
	
	List<PropertiesPojo> getCarouselProperties() throws RpcException;
	PropertiesPojo getPropertiesForMenu(String menuId) throws RpcException;
	PropertiesPojo getSiteSpecificTextProperties() throws RpcException;
}
