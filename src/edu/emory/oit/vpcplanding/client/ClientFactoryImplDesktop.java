package edu.emory.oit.vpcplanding.client;

import com.google.gwt.activity.shared.ActivityManager;
import com.google.gwt.activity.shared.ActivityMapper;
import com.google.gwt.core.client.GWT;
import com.google.gwt.place.shared.PlaceController;
import com.google.gwt.place.shared.PlaceHistoryHandler;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.SimpleEventBus;

import edu.emory.oit.vpcplanding.client.activity.AppActivityMapper;
import edu.emory.oit.vpcplanding.client.activity.AppPlaceHistoryMapper;
import edu.emory.oit.vpcplanding.client.desktop.DesktopAppShell;
import edu.emory.oit.vpcplanding.client.desktop.DesktopMainLanding;
import edu.emory.oit.vpcplanding.client.presenter.landing.MainLandingView;

public class ClientFactoryImplDesktop implements ClientFactory {
	private final EventBus eventBus = new SimpleEventBus();
	private final PlaceController placeController = new PlaceController(eventBus);
	private AppShell shell;
	private ActivityManager activityManager;

	private final AppPlaceHistoryMapper historyMapper = GWT.create(AppPlaceHistoryMapper.class);

	/**
	 * The stock GWT class that ties the PlaceController to browser history,
	 * configured by our custom {@link #historyMapper}.
	 */
	private final PlaceHistoryHandler historyHandler = new PlaceHistoryHandler(historyMapper);
	
	// the desktop view implementations
	MainLandingView mainLandingView;
	

    protected ActivityManager getActivityManager() {
        if (activityManager == null) {
            activityManager = new ActivityManager(createActivityMapper(), eventBus);
        }
        return activityManager;
    }
    /**
     * ActivityMapper determines an Activity to run for a particular place,
     * configures the {@link #getActivityManager()}
     */
    protected ActivityMapper createActivityMapper() {
        return new AppActivityMapper(this);
    }

	public void setActivityManager(ActivityManager activityManager) {
		this.activityManager = activityManager;
	}

	public EventBus getEventBus() {
		return eventBus;
	}

	public PlaceController getPlaceController() {
		return placeController;
	}

	public AppPlaceHistoryMapper getHistoryMapper() {
		return historyMapper;
	}

	public PlaceHistoryHandler getHistoryHandler() {
		return historyHandler;
	}

	@Override
	public AppBootstrapper getApp() {
		return new AppBootstrapper(eventBus, getPlaceController(),
				getActivityManager(), historyMapper, historyHandler, 
				getShell());
	}

	@Override
	public AppShell getShell() {
		if (shell == null) {
			shell = createShell();
		}
		return shell;
	}

	protected AppShell createShell() {
		return new DesktopAppShell(getEventBus(), this);
	}

	@Override
	public MainLandingView getMainLandingView() {
        if (mainLandingView == null) {
        		mainLandingView = createMainLandingView();
        }
        return mainLandingView;
	}
    protected MainLandingView createMainLandingView() {
        return new DesktopMainLanding();
    }
}
