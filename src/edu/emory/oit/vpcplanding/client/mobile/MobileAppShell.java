package edu.emory.oit.vpcplanding.client.mobile;

import java.util.logging.Logger;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.user.client.ui.IsWidget;
import com.google.gwt.user.client.ui.ResizeComposite;
import com.google.gwt.user.client.ui.Widget;
import com.google.web.bindery.event.shared.EventBus;

import edu.emory.oit.vpcplanding.client.AppShell;
import edu.emory.oit.vpcplanding.client.ClientFactory;
import edu.emory.oit.vpcplanding.shared.PropertiesPojo;

public class MobileAppShell extends ResizeComposite implements AppShell {
    Logger log=Logger.getLogger(MobileAppShell.class.getName());
    ClientFactory clientFactory;
    EventBus eventBus;

	private static MobileAppShellUiBinder uiBinder = GWT.create(MobileAppShellUiBinder.class);

	interface MobileAppShellUiBinder extends UiBinder<Widget, MobileAppShell> {
	}

	public MobileAppShell() {
		initWidget(uiBinder.createAndBindUi(this));
	}

	public MobileAppShell(final EventBus eventBus, ClientFactory clientFactory) {
		initWidget(uiBinder.createAndBindUi(this));
		
		this.clientFactory = clientFactory;
		this.eventBus = eventBus;
		GWT.log("Mobile shell...");
	}

	@Override
	public void setWidget(IsWidget w) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void initializeAwsServiceMap() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setLeavingPage(boolean leavingPage) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public PropertiesPojo getSiteSpecificProperties() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setSiteSpecificProperties(PropertiesPojo properties) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public String getSiteSpecificServiceName() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getSiteName() {
		// TODO Auto-generated method stub
		return null;
	}
}
