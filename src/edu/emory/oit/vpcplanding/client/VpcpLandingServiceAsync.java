package edu.emory.oit.vpcplanding.client;

import java.util.List;

import com.google.gwt.user.client.rpc.AsyncCallback;

import edu.emory.oit.vpcplanding.shared.AWSServiceQueryFilterPojo;
import edu.emory.oit.vpcplanding.shared.AWSServiceQueryResultPojo;
import edu.emory.oit.vpcplanding.shared.AWSServiceSummaryPojo;
import edu.emory.oit.vpcplanding.shared.PropertiesPojo;
import edu.emory.oit.vpcplanding.shared.ReleaseInfo;
import edu.emory.oit.vpcplanding.shared.RpcException;
import edu.emory.oit.vpcplanding.shared.ServiceSecurityAssessmentQueryFilterPojo;
import edu.emory.oit.vpcplanding.shared.ServiceSecurityAssessmentQueryResultPojo;
import edu.emory.oit.vpcplanding.shared.UserAccountPojo;

/**
 * The async counterpart of <code>VpcpLandingService</code>.
 */
public interface VpcpLandingServiceAsync {
	void getLoginURL(AsyncCallback<String> callback) throws RpcException;
	void getAWSServiceMap(AsyncCallback<AWSServiceSummaryPojo> callback) throws RpcException;
	void getServicesForFilter(AWSServiceQueryFilterPojo filter, AsyncCallback<AWSServiceQueryResultPojo> callback);
	void getAccountSeriesText(AsyncCallback<String> callback) throws RpcException;
	void getAwsConsoleInfoText(AsyncCallback<String> callback) throws RpcException;
	void getAwsConsoleURL(AsyncCallback<String> callback) throws RpcException;
	void getEmoryConsoleURL(AsyncCallback<String> callback) throws RpcException;
	void getCreateAccountURL(AsyncCallback<String> callback) throws RpcException;
	// ReleaseInfo
	void getReleaseInfo(AsyncCallback<ReleaseInfo> callback);
	// UserAccount services (user logged in)
	void getUserLoggedIn(AsyncCallback<UserAccountPojo> callback);
	void getEsbServiceStatusURL(AsyncCallback<String> callback);
	void getMonitoringStatusURL(AsyncCallback<String> callback);
	void getServiceOverviewURL(AsyncCallback<String> callback);
	void getUseCasesURL(AsyncCallback<String> callback);
	void getGettingStartedURL(AsyncCallback<String> callback);
	void getFaqURL(AsyncCallback<String> callback);
	void getSecurityURL(AsyncCallback<String> callback);
	void getSupportURL(AsyncCallback<String> callback);
	void getArchitectureURL(AsyncCallback<String> callback);
	void getPartnersURL(AsyncCallback<String> callback);
	void getGrantsURL(AsyncCallback<String> callback);
	void getTrainingURL(AsyncCallback<String> callback);
	void getFundingURL(AsyncCallback<String> callback);
	void getResourcesURL(AsyncCallback<String> callback);
	void getWelcomeURL(AsyncCallback<String> callback);

	void getSecurityAssessmentsForFilter(ServiceSecurityAssessmentQueryFilterPojo filter, AsyncCallback<ServiceSecurityAssessmentQueryResultPojo> callback);

	void getCarouselProperties(AsyncCallback<List<PropertiesPojo>> callback);
	void getPropertiesForMenu(String menuId, AsyncCallback<PropertiesPojo> callback);
	void getSiteSpecificTextProperties(AsyncCallback<PropertiesPojo> callback);
}
