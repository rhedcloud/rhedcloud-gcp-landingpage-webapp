package edu.emory.oit.vpcplanding.client;

import com.google.gwt.user.client.ui.AcceptsOneWidget;
import com.google.gwt.user.client.ui.IsWidget;

import edu.emory.oit.vpcplanding.shared.PropertiesPojo;

public interface AppShell extends AcceptsOneWidget, IsWidget {
	void initializeAwsServiceMap();
	void setLeavingPage(boolean leavingPage);
	PropertiesPojo getSiteSpecificProperties();
	void setSiteSpecificProperties(PropertiesPojo properties);
	String getSiteSpecificServiceName();
	String getSiteName();
}
