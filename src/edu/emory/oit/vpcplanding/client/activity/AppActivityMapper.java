package edu.emory.oit.vpcplanding.client.activity;

import com.google.gwt.activity.shared.AbstractActivity;
import com.google.gwt.activity.shared.Activity;
import com.google.gwt.activity.shared.ActivityMapper;
import com.google.gwt.core.shared.GWT;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.place.shared.Place;
import com.google.gwt.user.client.ui.AcceptsOneWidget;

import edu.emory.oit.vpcplanding.client.ClientFactory;
import edu.emory.oit.vpcplanding.client.presenter.landing.MainLandingPlace;
import edu.emory.oit.vpcplanding.client.presenter.landing.MainLandingPresenter;

public class AppActivityMapper implements ActivityMapper {
	private final ClientFactory clientFactory;

	public AppActivityMapper(ClientFactory clientFactory) {
		this.clientFactory = clientFactory;
	}

	@Override
	public Activity getActivity(final Place place) {
		if (place instanceof MainLandingPlace) {
			GWT.log("main landing place...");
			// The list of case records.
			return new AbstractActivity() {
				@Override
				public void start(AcceptsOneWidget panel, EventBus eventBus) {
					MainLandingPresenter presenter = new MainLandingPresenter(clientFactory, (MainLandingPlace) place);
					presenter.start(eventBus);
					panel.setWidget(presenter);
				}
			};
		}
		GWT.log("no place found...");
		return null;
	}

}
