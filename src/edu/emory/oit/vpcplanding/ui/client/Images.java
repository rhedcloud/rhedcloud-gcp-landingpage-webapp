package edu.emory.oit.vpcplanding.ui.client;

import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.ImageResource;

public interface Images extends ClientBundle {
	
	@Source("images/hamburger.png")
	ImageResource hamburger();

}
