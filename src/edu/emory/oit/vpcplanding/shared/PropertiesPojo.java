package edu.emory.oit.vpcplanding.shared;

import java.util.HashMap;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.IsSerializable;

@SuppressWarnings("serial")
public class PropertiesPojo extends SharedObject implements IsSerializable, Comparable<PropertiesPojo> {
	HashMap<String, String> propertyMap = new HashMap<String, String>();
	public PropertiesPojo() {
		
	}
	
	public void setProperty(String key, String value) {
		propertyMap.put(key, value);
	}
	public String getProperty(String key) {
		return propertyMap.get(key);
	}
	public String getProperty(String key, String defaultValue) {
		String value = propertyMap.get(key);
		if (value == null) {
			return defaultValue;
		}
		return value;
	}

	@Override
	public int compareTo(PropertiesPojo o) {
		String o1SortOrder = o.getProperty("sortOrder", null);
		String o2SortOrder = getProperty("sortOrder", null);
		if (o1SortOrder != null && o2SortOrder != null) {
			try {
				int sort1 = Integer.parseInt(o1SortOrder);
				int sort2 = Integer.parseInt(o2SortOrder);
				if (sort1 == sort2) {
					return 0;
				}
				if (sort1 > sort2) {
					return -1;
				}
				return 1;
			}
			catch (Exception e) {
				return 0;
			}
		}
		return 0;
	}
}
