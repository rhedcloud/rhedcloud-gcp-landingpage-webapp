package edu.emory.oit.vpcplanding.shared;

import com.google.gwt.user.client.rpc.IsSerializable;

@SuppressWarnings("serial")
public class AWSTagPojo extends SharedObject implements IsSerializable {
	String key;
	String value;

	public AWSTagPojo() {
	}

	public AWSTagPojo(String key, String value) {
		this.key = key;
		this.value = value;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

}
