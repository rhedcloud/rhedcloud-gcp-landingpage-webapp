package edu.emory.oit.vpcplanding.server;

import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.TimeZone;
import java.util.logging.Logger;

import javax.jms.JMSException;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.ws.rs.GET;
import javax.ws.rs.Path;

import org.openeai.config.AppConfig;
import org.openeai.config.EnterpriseConfigurationObjectException;
import org.openeai.config.EnterpriseFieldException;
import org.openeai.config.PropertyConfig;
import org.openeai.jms.producer.PointToPointProducer;
import org.openeai.jms.producer.ProducerPool;
import org.openeai.moa.EnterpriseObjectQueryException;
import org.openeai.moa.XmlEnterpriseObject;
import org.openeai.moa.XmlEnterpriseObjectException;
import org.openeai.transport.RequestService;
import org.openeai.utils.config.AppConfigFactory;
import org.openeai.utils.config.AppConfigFactoryException;
import org.openeai.utils.config.SimpleAppConfigFactory;

import com.amazon.aws.moa.jmsobjects.services.v1_0.ServiceSecurityAssessment;
import com.amazon.aws.moa.objects.resources.v1_0.SecurityRisk;
import com.amazon.aws.moa.objects.resources.v1_0.ServiceControl;
import com.amazon.aws.moa.objects.resources.v1_0.ServiceQuerySpecification;
import com.amazon.aws.moa.objects.resources.v1_0.ServiceSecurityAssessmentQuerySpecification;
import com.amazon.aws.moa.objects.resources.v1_0.ServiceTest;
import com.amazon.aws.moa.objects.resources.v1_0.ServiceTestPlan;
import com.amazon.aws.moa.objects.resources.v1_0.ServiceTestRequirement;
import com.amazon.aws.moa.objects.resources.v1_0.ServiceTestStep;
import com.google.gwt.user.server.rpc.RemoteServiceServlet;

import edu.emory.oit.vpcplanding.client.VpcpLandingService;
import edu.emory.oit.vpcplanding.shared.AWSServicePojo;
import edu.emory.oit.vpcplanding.shared.AWSServiceQueryFilterPojo;
import edu.emory.oit.vpcplanding.shared.AWSServiceQueryResultPojo;
import edu.emory.oit.vpcplanding.shared.AWSServiceSummaryPojo;
import edu.emory.oit.vpcplanding.shared.AWSTagPojo;
import edu.emory.oit.vpcplanding.shared.Cache;
import edu.emory.oit.vpcplanding.shared.Constants;
import edu.emory.oit.vpcplanding.shared.PropertiesPojo;
import edu.emory.oit.vpcplanding.shared.ReleaseInfo;
import edu.emory.oit.vpcplanding.shared.RpcException;
import edu.emory.oit.vpcplanding.shared.SecurityRiskPojo;
import edu.emory.oit.vpcplanding.shared.ServiceControlPojo;
import edu.emory.oit.vpcplanding.shared.ServiceSecurityAssessmentPojo;
import edu.emory.oit.vpcplanding.shared.ServiceSecurityAssessmentQueryFilterPojo;
import edu.emory.oit.vpcplanding.shared.ServiceSecurityAssessmentQueryResultPojo;
import edu.emory.oit.vpcplanding.shared.ServiceTestPlanPojo;
import edu.emory.oit.vpcplanding.shared.ServiceTestPojo;
import edu.emory.oit.vpcplanding.shared.ServiceTestRequirementPojo;
import edu.emory.oit.vpcplanding.shared.ServiceTestStepPojo;
import edu.emory.oit.vpcplanding.shared.SharedObject;
import edu.emory.oit.vpcplanding.shared.UUID;
import edu.emory.oit.vpcplanding.shared.UserAccountPojo;

/**
 * The server-side implementation of the RPC service.
 */
@Path("vpcpLandingHealthCheck")
@SuppressWarnings("serial")
public class VpcpLandingServiceImpl extends RemoteServiceServlet implements VpcpLandingService {

	private static final String GENERAL_PROPERTIES = "GeneralProperties";
	private static final String URL_PROPERTIES = "URLProperties";
	private static final String AWS_SERVICE_NAME = "AWSRequestService";
	private static final String MENU_PROPERTIES = "MenuProperties";
	private static final String SITE_SPECIFIC_TEXT_PROPERTIES = "SiteSpecificTextProperties";

	private Logger log = Logger.getLogger(getClass().getName());
	private String baseLoginURL = null;

	private Object lock = new Object();
	private boolean initializing = false;
	private boolean useEsbService = true;
	private boolean useShibboleth = true;
	private boolean useAuthzService = true;
	private static AppConfig appConfig = null;
	Properties generalProps = null;
	private String configDocPath = null;
	private String appId = null;
	String createAccountURL = null;
	String emoryConsoleURL = null;
	String awsConsoleURL = null;
	private String applicationEnvironment=null;
	String awsConsoleInfoText = null;

	private ProducerPool awsProducerPool = null;
	private int defaultRequestTimeoutInterval = 20000;
	AWSServiceSummaryPojo serviceSummary;

	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);

		initAppConfig();

		ReleaseInfo ri = this.getReleaseInfo();
		info(ri.toString() + " initialization complete.");
	}

	private void initAppConfig() throws ServletException {
		info("initAppConfig started...");
		synchronized (lock) {
			if (initializing) {
				System.out.println("initialization is already underway...");
				return;
			} else {
				initializing = true;
			}
		}

		try {
			String s = this.getServletConfig()
					.getInitParameter("useEsbService");
			if (s == null) {
				s = "true";
			}
			useEsbService = Boolean.parseBoolean(s);

			configDocPath = System.getProperty("configDocPath");
			if (configDocPath == null) {
				configDocPath = System.getenv("configDocPath");
			}
			if (configDocPath == null) {
				configDocPath = this.getServletConfig().getInitParameter(
						"configDocPath");
			}
			if (configDocPath == null) {
				configDocPath = "Unknown configDocPath";
			}

			appId = System.getProperty("appId");
			if (appId == null) {
				appId = System.getenv("appId");
			}
			if (appId == null) {
				appId = this.getServletConfig().getInitParameter("appId");
			}
			if (appId == null) {
				appId = "Unknown appId";
			}

			info("useEsbService is: " + useEsbService);
			info("configDocPath is: " + configDocPath);
			info("appId is: " + appId);
		} 
		catch (Throwable e) {
			e.printStackTrace();
			throw new ServletException(e);
		}

		if (!useEsbService) {
			System.out.println("Not using AppConfig.");
			return;
		}

		if (getAppConfig() != null) {
			System.out.println("App config is already initialized...");
			return;
		}

		try {
			info("Initializing AppConfig...");
			AppConfigFactory acf = new SimpleAppConfigFactory();
			setAppConfig(acf.makeAppConfig(configDocPath, appId));
			info("AppConfig initialized...");
			
			awsProducerPool = (ProducerPool) getAppConfig().getObject(
					AWS_SERVICE_NAME);
//			this.getAWSServiceMap();
			
			generalProps = getAppConfig().getProperties(GENERAL_PROPERTIES);
			
			applicationEnvironment = generalProps.getProperty("applicationEnvironment", "DEV");
			this.getAwsConsoleURL();
			this.getEmoryConsoleURL();
			this.getCreateAccountURL();
		} 
		catch (EnterpriseConfigurationObjectException e) {
			e.printStackTrace();
			throw new ServletException(e);
		} 
		catch (AppConfigFactoryException e) {
			e.printStackTrace();
			throw new ServletException(e);
		}

		info("initAppConfig ended...");
	}

	/**
	 * Escape an html string. Escaping data received from the client helps to
	 * prevent cross-site script vulnerabilities.
	 * 
	 * @param html the html string to escape
	 * @return the escaped string
	 */
	private String escapeHtml(String html) {
		if (html == null) {
			return null;
		}
		return html.replaceAll("&", "&amp;").replaceAll("<", "&lt;").replaceAll(">", "&gt;");
	}

	@Override
	public String getLoginURL() throws RpcException {
		HttpServletRequest r = this.getThreadLocalRequest();
		String referer = r.getHeader("Referer");
		info("referrer is; " + referer);
		info("baseLoginURL is: " + getBaseLoginURL());

		String loginURL = getBaseLoginURL() + "?target=" + referer;
		info("returning a loginURL of: '" + loginURL + "'");
		
		return loginURL;
	}

	public String getBaseLoginURL() {
		return baseLoginURL;
	}

	public void setBaseLoginURL(String baseLoginURL) {
		this.baseLoginURL = baseLoginURL;
	}

	private void info(String msg) {
		log.info(msg);
	}

	@Override
	public ServiceSecurityAssessmentQueryResultPojo getSecurityAssessmentsForFilter(
			ServiceSecurityAssessmentQueryFilterPojo filter) throws RpcException {
		
		ServiceSecurityAssessmentQueryResultPojo result = new ServiceSecurityAssessmentQueryResultPojo();
		result.setFilterUsed(filter);
		List<ServiceSecurityAssessmentPojo> pojos = new java.util.ArrayList<ServiceSecurityAssessmentPojo>();
		try {
			ServiceSecurityAssessmentQuerySpecification queryObject = (ServiceSecurityAssessmentQuerySpecification) getObject(Constants.MOA_SVC_SECURITY_ASSESSMENT_QUERY_SPEC);
			ServiceSecurityAssessment actionable = (ServiceSecurityAssessment) getObject(Constants.MOA_SVC_SECURITY_ASSESSMENT);

			if (filter != null) {
				queryObject.setServiceId(filter.getServiceId());
				queryObject.setServiceSecurityAssessmentId(filter.getAssessmentId());
			}

//			String authUserId = this.getAuthUserIdForHALS();
//			actionable.getAuthentication().setAuthUserId(authUserId);
//			info("[getSecurityAssessmentsForFilter] AuthUserId is: " + actionable.getAuthentication().getAuthUserId());

			@SuppressWarnings("unchecked")
			List<ServiceSecurityAssessment> moas = actionable.query(queryObject,
					this.getAWSRequestService());
			info("[getSecurityAssessmentsForFilter] got " + moas.size() + 
					" Security Assessments from ESB service" + 
					(filter != null ? " for filter: " + filter.toString() : ""));
			for (ServiceSecurityAssessment moa : moas) {
				info("[query] assessment as xml string: " + moa.toXmlString());
				ServiceSecurityAssessmentPojo pojo = new ServiceSecurityAssessmentPojo();
				ServiceSecurityAssessmentPojo baseline = new ServiceSecurityAssessmentPojo();
				this.populateSecurityAssessmentPojo(moa, pojo);
				this.populateSecurityAssessmentPojo(moa, baseline);
				pojo.setBaseline(baseline);
				pojos.add(pojo);
			}

			Collections.sort(pojos);
			result.setResults(pojos);
			result.setFilterUsed(filter);
			return result;
		} 
		catch (EnterpriseConfigurationObjectException e) {
			e.printStackTrace();
			throw new RpcException(e);
		} 
		catch (EnterpriseFieldException e) {
			e.printStackTrace();
			throw new RpcException(e);
		} 
		catch (EnterpriseObjectQueryException e) {
			e.printStackTrace();
			throw new RpcException(e);
		} 
		catch (JMSException e) {
			e.printStackTrace();
			throw new RpcException(e);
		} 
		catch (XmlEnterpriseObjectException e) {
			e.printStackTrace();
			throw new RpcException(e);
		} 
	}

	@SuppressWarnings("unchecked")
	private void populateSecurityAssessmentPojo(ServiceSecurityAssessment moa, ServiceSecurityAssessmentPojo pojo) throws XmlEnterpriseObjectException {
		pojo.setServiceSecurityAssessmentId(moa.getServiceSecurityAssessmentId());
		pojo.setStatus(moa.getStatus());
		if (moa.getServiceId() != null) {
			for (String svcid : (List<String>)moa.getServiceId()) {
				pojo.getServiceIds().add(svcid);
			}
		}
		if (moa.getSecurityRisk() != null) {
			for (SecurityRisk risk : (List<SecurityRisk>)moa.getSecurityRisk()) {
				SecurityRiskPojo rp = new SecurityRiskPojo();
				rp.setSecurityRiskId(risk.getSecurityRiskId());
				rp.setServiceId(risk.getServiceId());
				rp.setSequenceNumber(this.toIntFromString(risk.getSequenceNumber()));
				rp.setSecurityRiskName(risk.getServiceRiskName());
				rp.setRiskLevel(risk.getRiskLevel());
				rp.setDescription(risk.getDescription());
				rp.setAssessorId(risk.getAssessorId());
				rp.setAssessmentDate(this.toDateFromDatetime(risk.getAssessmentDatetime()));
				if (risk.getServiceControl() != null) {
					for (ServiceControl sc : (List<ServiceControl>)risk.getServiceControl()) {
						ServiceControlPojo scp = new ServiceControlPojo();
						scp.setServiceId(sc.getServiceId());
						scp.setServiceControlId(sc.getServiceControlId());
						scp.setSequenceNumber(this.toIntFromString(sc.getSequenceNumber()));
						scp.setServiceControlName(sc.getServiceControlName());
						scp.setDescription(sc.getDescription());
						scp.setAssessorId(sc.getAssessorId());
						scp.setAssessmentDate(this.toDateFromDatetime(sc.getAssessmentDatetime()));
						scp.setVerifier(sc.getVerifier());
						scp.setImplementationType(sc.getImplementationType());
						scp.setControlType(sc.getControlType());
						if (sc.getDocumentationUrl() != null) {
							for (String docUrl : (List<String>)sc.getDocumentationUrl()) {
								scp.getDocumentationUrls().add(docUrl);
							}
						}
						if (sc.getVerificationDatetime() != null) {
							scp.setVerificationDate(this.toDateFromDatetime(sc.getVerificationDatetime()));
						}
						rp.getServiceControls().add(scp);
					}
					Collections.sort(rp.getServiceControls());
				}
				pojo.getSecurityRisks().add(rp);
			}
		}
//		if (moa.getServiceControl() != null) {
//			for (ServiceControl sc : (List<ServiceControl>)moa.getServiceControl()) {
//				ServiceControlPojo scp = new ServiceControlPojo();
//				scp.setServiceId(sc.getServiceId());
//				scp.setServiceControlId(sc.getServiceControlId());
//				scp.setSequenceNumber(this.toIntFromString(sc.getSequenceNumber()));
//				scp.setServiceControlName(sc.getServiceControlName());
//				scp.setDescription(sc.getDescription());
//				scp.setAssessorId(sc.getAssessorId());
//				scp.setAssessmentDate(this.toDateFromDatetime(sc.getAssessmentDatetime()));
//				scp.setVerifier(sc.getVerifier());
//				if (sc.getVerificationDatetime() != null) {
//					scp.setVerificationDate(this.toDateFromDatetime(sc.getVerificationDatetime()));
//				}
//				pojo.getServiceControls().add(scp);
//			}
//		}
//		if (moa.getServiceGuideline() != null) {
//			for (ServiceGuideline sg : (List<ServiceGuideline>)moa.getServiceGuideline()) {
//				ServiceGuidelinePojo sgp = new ServiceGuidelinePojo();
//				sgp.setServiceId(sg.getServiceId());
//				sgp.setSequenceNumber(this.toIntFromString(sg.getSequenceNumber()));
//				sgp.setServiceGuidelineName(sg.getServiceGuidelineName());
//				sgp.setDescription(sg.getDescription());
//				sgp.setAssessorId(sg.getAssessorId());
//				sgp.setAssessmentDate(this.toDateFromDatetime(sg.getAssessmentDatetime()));
//				pojo.getServiceGuidelines().add(sgp);
//			}
//		}
		if (moa.getServiceTestPlan() != null) {
			ServiceTestPlan stpm = moa.getServiceTestPlan();
			ServiceTestPlanPojo stpp = new ServiceTestPlanPojo();
			stpp.setServiceId(stpm.getServiceId());
			// test plan requirement list
			if (stpm.getServiceTestRequirement() != null) {
				for (ServiceTestRequirement strm : (List<ServiceTestRequirement>)stpm.getServiceTestRequirement()) {
					ServiceTestRequirementPojo strp = new ServiceTestRequirementPojo();
					strp.setServiceTestRequirementId(strm.getServiceTestRequirementId());
					strp.setSequenceNumber(this.toIntFromString(strm.getSequenceNumber()));
					strp.setDescription(strm.getDescription());
					if (strm.getServiceTest() != null) {
						for (ServiceTest stm : (List<ServiceTest>)strm.getServiceTest()) {
							ServiceTestPojo stp = new ServiceTestPojo();
							stp.setServiceTestId(stm.getServiceTestId());
							stp.setSequenceNumber(this.toIntFromString(stm.getSequenceNumber()));
							stp.setDescription(stm.getDescription());
							stp.setServiceTestExpectedResult(stm.getServiceTestExpectedResult());
							if (stm.getServiceTestStep() != null) {
								for (ServiceTestStep stsm : (List<ServiceTestStep>)stm.getServiceTestStep()) {
									ServiceTestStepPojo stsp = new ServiceTestStepPojo();
									stsp.setServiceTestStepId(stsm.getServiceTestStepId());
									stsp.setSequenceNumber(this.toIntFromString(stsm.getSequenceNumber()));
									stsp.setDescription(stsm.getDescription());
									stp.getServiceTestSteps().add(stsp);
								}
							}
							strp.getServiceTests().add(stp);
						}
					}
					stpp.getServiceTestRequirements().add(strp);
				}
			}
			pojo.setServiceTestPlan(stpp);
		}
	}

	@SuppressWarnings("unchecked")
	private void populateAWSServicePojo(com.amazon.aws.moa.jmsobjects.services.v1_0.Service moa, AWSServicePojo pojo) throws XmlEnterpriseObjectException {
		pojo.setServiceId(moa.getServiceId());
		pojo.setAwsServiceCode(moa.getAwsServiceCode());
		pojo.setAwsServiceName(moa.getAwsServiceName());
		pojo.setCombinedServiceName(moa.getCombinedServiceName());
		pojo.setAlternateServiceName(moa.getAlternateServiceName());
		pojo.setAwsStatus(moa.getAwsStatus());
		pojo.setSiteStatus(moa.getSiteStatus());
		pojo.setAwsLandingPageUrl(moa.getAwsServiceLandingPageUrl());
		pojo.setSiteLandingPageUrl(moa.getSiteServiceLandingPageUrl());
		pojo.setDescription(moa.getDescription());
		pojo.setAwsHipaaEligible(moa.getAwsHipaaEligible());
		pojo.setSiteHipaaEligible(moa.getSiteHipaaEligible());
		if (moa.getConsoleCategoryLength() > 0) {
			for (String consoleCat : (List<String>)moa.getConsoleCategory()) {
				pojo.getConsoleCategories().add(consoleCat);
			}
		}
		if (moa.getCategoryLength() > 0) {
			for (String cat : (List<String>)moa.getCategory()) {
				pojo.getAwsCategories().add(cat);
			}
		}
		if (moa.getTagLength() > 0) {
			for (com.amazon.aws.moa.objects.resources.v1_0.Tag tag : 
				(List<com.amazon.aws.moa.objects.resources.v1_0.Tag>)moa.getTag()) {
				
				AWSTagPojo ptag = new AWSTagPojo();
				ptag.setKey(tag.getKey());
				ptag.setValue(tag.getValue());
				pojo.getTags().add(ptag);
			}
		}
		
		this.setPojoCreateInfo(pojo, moa);
		this.setPojoUpdateInfo(pojo, moa);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public AWSServiceQueryResultPojo getServicesForFilter(AWSServiceQueryFilterPojo filter) throws RpcException {
		AWSServiceQueryResultPojo result = new AWSServiceQueryResultPojo();
		result.setFilterUsed(filter);
		List<AWSServicePojo> pojos = new java.util.ArrayList<AWSServicePojo>();

		try {
			com.amazon.aws.moa.jmsobjects.services.v1_0.Service actionable = 
					(com.amazon.aws.moa.jmsobjects.services.v1_0.Service) getObject(Constants.MOA_SERVICE);
			ServiceQuerySpecification queryObject = (ServiceQuerySpecification) getObject(Constants.MOA_SERVICE_QUERY_SPEC);
			
			if (filter != null) {
				if (!filter.isFuzzyFilter()) {
					queryObject.setServiceId(filter.getServiceId());
					queryObject.setAwsServiceCode(filter.getAwsServiceCode());
					queryObject.setAwsServiceName(filter.getAwsServiceName());
					queryObject.setAwsStatus(filter.getAwsStatus());
					queryObject.setSiteStatus(filter.getSiteStatus());
					queryObject.setAwsHipaaEligible(filter.getAwsHipaaEligible());
					queryObject.setSiteHipaaEligible(filter.getSiteHipaaEligible());
					for (String consoleCat : filter.getConsoleCategories()) {
						queryObject.addConsoleCategory(consoleCat);
					}
					for (String cat : filter.getCategories()) {
						queryObject.addCategory(cat);
					}
				}
				else {
					info("[getServicesForFilter] performing a fuzzy filter...");
				}
			}

			info("[getServicesForFilter] query object is: " + queryObject.toXmlString());
			List<com.amazon.aws.moa.jmsobjects.services.v1_0.Service> moas = actionable.query(queryObject,
					this.getAWSRequestService());
			info("[getServicessForFilter] got " + moas.size() + " services from ESB service");

			for (com.amazon.aws.moa.jmsobjects.services.v1_0.Service service : moas) {
				if (filter != null && filter.isFuzzyFilter()) {
					boolean includeInList=false;
					if (filter.getAwsServiceName() != null && filter.getAwsServiceName().length() > 0) {
						if (service.getAwsServiceName().toLowerCase().indexOf(filter.getAwsServiceName().toLowerCase()) >= 0) {
							includeInList = true;
						}
					}
					else if (filter.getConsoleCategories().size() > 0) {
						String filterCat = filter.getConsoleCategories().get(0);
						for (String consoleCat : (List<String>)service.getConsoleCategory()) {
							if (consoleCat.toLowerCase().indexOf(filterCat.toLowerCase()) >= 0) {
								includeInList = true;
							}
						}
					}
					if (includeInList) {
						AWSServicePojo pojo = new AWSServicePojo();
						AWSServicePojo baseline = new AWSServicePojo();
						this.populateAWSServicePojo(service, pojo);
						this.populateAWSServicePojo(service, baseline);
						pojo.setBaseline(baseline);

						pojos.add(pojo);
					}
				}
				else {
					AWSServicePojo pojo = new AWSServicePojo();
					AWSServicePojo baseline = new AWSServicePojo();
					this.populateAWSServicePojo(service, pojo);
					this.populateAWSServicePojo(service, baseline);
					pojo.setBaseline(baseline);

					pojos.add(pojo);
				}
			}
			
			Collections.sort(pojos);
			result.setResults(pojos);
		} 
		catch (EnterpriseConfigurationObjectException e) {
			e.printStackTrace();
			throw new RpcException(e);
		} 
		catch (XmlEnterpriseObjectException e) {
			e.printStackTrace();
			throw new RpcException(e);
		} 
		catch (EnterpriseObjectQueryException e) {
			e.printStackTrace();
			throw new RpcException(e);
		} 
		catch (JMSException e) {
			e.printStackTrace();
			throw new RpcException(e);
		} 
		catch (EnterpriseFieldException e) {
			e.printStackTrace();
			throw new RpcException(e);
		}

		info("returning " + result.getResults().size() + " services.");
		return result;
	}

	@Override
	public AWSServiceSummaryPojo getAWSServiceMap() throws RpcException {
		info("checking cache for existing service summary...");
		serviceSummary = (AWSServiceSummaryPojo) Cache.getCache().get(
				Constants.SERVICE_SUMMARY + getCurrentSessionId());

		if (serviceSummary == null) {
			info("no service summary found in cache, building service summary.");
		    serviceSummary = new AWSServiceSummaryPojo();
		}
		else {
			// check how long it's been there and refresh if it's "stale"
			java.util.Date currentTime = new java.util.Date();
			if (serviceSummary.getUpdateTime() != null) {
				long millisSinceUpdated = currentTime.getTime() - serviceSummary.getUpdateTime().getTime();
				// if it's less than one minute, don't refresh
				if (millisSinceUpdated <= 60000) {
					info("service summary found in cache and it's NOT stale, returning cached service summary");
				    int statCount = serviceSummary.getAwsServiceStatistics().size() + serviceSummary.getSiteServiceStatistics().size();
				    info("[getAWSServiceMap] there are " + statCount + " service stats in the cached summary.");
					return serviceSummary;
				}
				info("service summary was found in cached but it's stale, refreshing service summary");
			}
			info("service summary was found in cache but it has no last updated time, refreshing service summary");
		}
		
		int svcCnt = 0;

		AWSServiceQueryResultPojo servicesResult = this.getServicesForFilter(null);
		
		// key is category
		HashMap<String, List<AWSServicePojo>> awsServicesMap = 
			new HashMap<String, List<AWSServicePojo>>();
		
		svcCnt = servicesResult.getResults().size();
		for (AWSServicePojo service : servicesResult.getResults()) {
	    	// see if this category is already in the awsServicesMap
			String category = "Unknown";
			if (service.getConsoleCategories() != null && 
				service.getConsoleCategories().size() > 0) {
				
				for (String l_category : service.getConsoleCategories()) {
					this.addServiceToCategory(l_category, service, awsServicesMap);
				}
			}
			else if (service.getAwsCategories() != null && 
					service.getAwsCategories().size() > 0) {
				
				// have to check all the categories they're in...not just the fist one
				for (String l_category : service.getAwsCategories()) {
					this.addServiceToCategory(l_category, service, awsServicesMap);
				}
			}
			else {
				// add service to the unknown category
			}
		}
	    info("[getAWSServiceMap] returning " + svcCnt +" services in " + awsServicesMap.size() + " categories of services.");
	    serviceSummary.setServiceList(servicesResult.getResults());
	    serviceSummary.setServiceMap(awsServicesMap);
	    
	    PropertiesPojo siteProperties = this.getSiteSpecificTextProperties();
		String siteName = siteProperties.getProperty("siteName", "Emory");
		serviceSummary.setSiteName(siteName);

	    serviceSummary.initializeStatistics();
	    int statCount = serviceSummary.getAwsServiceStatistics().size() + serviceSummary.getSiteServiceStatistics().size();
	    info("[getAWSServiceMap] there are " + statCount + " service stats in the summary.");
	    serviceSummary.setUpdateTime(new java.util.Date());
		Cache.getCache().put(Constants.SERVICE_SUMMARY + getCurrentSessionId(), serviceSummary);
		return serviceSummary;
	}

	private void addServiceToCategory(String category, AWSServicePojo service, HashMap<String, List<AWSServicePojo>> awsServicesMap) {
		List<AWSServicePojo> servicesForCat = awsServicesMap.get(category);
		if (servicesForCat == null) {
			servicesForCat = new java.util.ArrayList<AWSServicePojo>();
			servicesForCat.add(service);
			awsServicesMap.put(category, servicesForCat);
		}
		else {
			// need to see if a service by this combined name, alternate name, aws name 
			// exists and if it does, don't add it
			boolean doAdd = true;
			svcLoop: for (AWSServicePojo existingSvc : servicesForCat) {
				if (existingSvc.getCombinedServiceName() != null && 
   					existingSvc.getCombinedServiceName().length() > 0 &&
					existingSvc.getCombinedServiceName().equalsIgnoreCase(service.getCombinedServiceName())) {
					doAdd = false;
					break svcLoop;
				}
				else if (existingSvc.getAlternateServiceName() != null && 
   					existingSvc.getAlternateServiceName().length() > 0 &&
   					existingSvc.getAlternateServiceName().equalsIgnoreCase(service.getAlternateServiceName())) {
					doAdd = false;
					break svcLoop;
    			}
			}
			if (doAdd) {
    			servicesForCat.add(service);
			}
		}
	}

	public boolean isInitializing() {
		return initializing;
	}

	public void setInitializing(boolean initializing) {
		this.initializing = initializing;
	}

	public boolean isUseEsbService() {
		return useEsbService;
	}

	public void setUseEsbService(boolean useEsbService) {
		this.useEsbService = useEsbService;
	}

	public boolean isUseShibboleth() {
		return useShibboleth;
	}

	public void setUseShibboleth(boolean useShibboleth) {
		this.useShibboleth = useShibboleth;
	}

	public boolean isUseAuthzService() {
		return useAuthzService;
	}

	public void setUseAuthzService(boolean useAuthzService) {
		this.useAuthzService = useAuthzService;
	}

	public AppConfig getAppConfig() {
		return appConfig;
	}

	public void setAppConfig(AppConfig appConfig) {
		this.appConfig = appConfig;
	}

	public Properties getGeneralProps() {
		return generalProps;
	}

	public void setGeneralProps(Properties generalProps) {
		this.generalProps = generalProps;
	}

	public String getConfigDocPath() {
		return configDocPath;
	}

	public void setConfigDocPath(String configDocPath) {
		this.configDocPath = configDocPath;
	}

	public String getAppId() {
		return appId;
	}

	public void setAppId(String appId) {
		this.appId = appId;
	}

	@Override
	public String getAwsConsoleURL() throws RpcException {
		try {
			Properties props = getAppConfig().getProperties(URL_PROPERTIES);
			this.awsConsoleURL = props.getProperty("awsConsoleURL", null);
			if (awsConsoleURL == null) {
				throw new RpcException("Null 'awsConsoleURL' property.  This application is not configured correctly.");
			}
			return awsConsoleURL;
		} catch (EnterpriseConfigurationObjectException e) {
			e.printStackTrace();
			throw new RpcException(e.getMessage());
		}
	}

	@Override
	public String getEmoryConsoleURL() throws RpcException {
		try {
			Properties props = getAppConfig().getProperties(URL_PROPERTIES);
			this.emoryConsoleURL = props.getProperty("emoryConsoleURL", null);
			if (emoryConsoleURL == null) {
				throw new RpcException("Null 'emoryConsoleURL' property.  This application is not configured correctly.");
			}
			return emoryConsoleURL;
		} catch (EnterpriseConfigurationObjectException e) {
			e.printStackTrace();
			throw new RpcException(e.getMessage());
		}
	}

	@Override
	public String getCreateAccountURL() throws RpcException {
		try {
			Properties props = getAppConfig().getProperties(URL_PROPERTIES);
			createAccountURL = props.getProperty("createAccountURL", null);
			if (createAccountURL == null) {
				throw new RpcException("Null 'createAccountURL' property.  This application is not configured correctly.");
			}
			return createAccountURL;
		} catch (EnterpriseConfigurationObjectException e) {
			e.printStackTrace();
			throw new RpcException(e.getMessage());
		}
	}

	@Override
	public ReleaseInfo getReleaseInfo() throws RpcException {
		ReleaseInfo ri = new ReleaseInfo();
		ri.setApplicationEnvironment(applicationEnvironment);
		return ri;
	}

	@Override
	public UserAccountPojo getUserLoggedIn() throws RpcException {
		// if the user account has been cached, we'll use it
		info("checking cache for existing user...");
		UserAccountPojo user = (UserAccountPojo) Cache.getCache().get(
				Constants.USER_ACCOUNT + getCurrentSessionId());

		if (user != null) {
			info("[found user in cache] user logged in is: " + user.getEppn());

			this.createSession(user.getEppn());
			Cache.getCache().put(Constants.USER_ACCOUNT + getCurrentSessionId(), user);
			return user;
		}
		else {
			info("no user currently logged in, checking for Shibboleth information");
			HttpServletRequest request = this.getThreadLocalRequest();

			String eppn = (String) request.getHeader("eduPersonPrincipalName");
			if (eppn == null) {
				eppn = (String) request.getAttribute("eduPersonPrincipalName");
			}

			if (eppn != null) {
				info("found eppn in Shibboleth attributes.  eppn is: '" + eppn + "'");
				if (eppn.trim().length() == 0) {
					info("found shibboleth eppn attribute but value is blank.");
					return null;
				}
				user = new UserAccountPojo();
				user.setEppn(eppn);
				this.createSession(user.getEppn());
				Cache.getCache().put(Constants.USER_ACCOUNT + getCurrentSessionId(), user);
			}
			return user;
		}
	}

	private String getCurrentSessionId() {
		HttpServletRequest request = this.getThreadLocalRequest();
		if (request != null) {
			HttpSession session = request.getSession();
			if (session != null) {
				info("session is not null...");
				return session.getId();
			}
			else {
				info("session is null...");
				return UUID.uuid();
			}
		} 
		else {
			// this may happen if createUser is called outside of an HTTP
			// request
			info("request is null...");
			return UUID.uuid();
		}
	}

	void createSession(String userId) {
		// (12 hours)
		//				info("Creating session for user " + userId + 
		//					" with an expiration time of " + sessionTimeoutIntervalSeconds + 
		//					" seconds.");
		//				getThreadLocalRequest().getSession().setMaxInactiveInterval(sessionTimeoutIntervalSeconds);

		// never expires
		info("Creating session for user " + userId + 
				" with an expiration time of -1 (never expires)");
		getThreadLocalRequest().getSession().setMaxInactiveInterval(-1);
		getThreadLocalRequest().getSession().setAttribute("UserID", userId);
	}

	@Override
	public String getAwsConsoleInfoText() throws RpcException {
		try {
			Properties props = getAppConfig().getProperties(URL_PROPERTIES);
			String l_awsConsoleURL = props.getProperty("awsConsoleURL", "Unknown");
			String l_awsConsoleInfoText = props.getProperty("awsConsoleInfoText", "Unknown");
			this.awsConsoleInfoText = l_awsConsoleURL + " / " + l_awsConsoleInfoText;
			return awsConsoleInfoText;
		} catch (EnterpriseConfigurationObjectException e) {
			e.printStackTrace();
			throw new RpcException(e.getMessage());
		}
	}

	@Override
	public String getAccountSeriesText() throws RpcException {
		try {
			Properties props = getAppConfig().getProperties(URL_PROPERTIES);
			String l_awsConsoleInfoText = props.getProperty("awsConsoleInfoText", "Unknown");
			return l_awsConsoleInfoText;
		} catch (EnterpriseConfigurationObjectException e) {
			e.printStackTrace();
			throw new RpcException(e.getMessage());
		}
	}
	
	
	@GET
	public String getVpcpLandingHealthCheck() {
		info("getVpcpLandingHealthCheck...");
		
		Properties props;
		try {
			props = getAppConfig().getProperties(URL_PROPERTIES);
		} catch (EnterpriseConfigurationObjectException e) {
			e.printStackTrace();
			throw new RpcException("Error");
		}
		AWSServiceSummaryPojo svcSummary = this.getAWSServiceMap(); 
		applicationEnvironment = props.getProperty("applicationEnvironment", "DEV");
		info("Account series text: " + this.getAccountSeriesText());
		info("appId: " + this.getAppId());
		info("AWS Console Info Text: " + this.getAwsConsoleInfoText());
		info("AWS service map: " + svcSummary.getServiceMap());
		info("appConfig is: " + appConfig);
		info("AppConfig stats: " + this.getAppConfig().dumpStats());
		info("Application Environment is: " + applicationEnvironment);

		if (applicationEnvironment != null && 
			this.getAwsConsoleURL() != null && 
			this.getEmoryConsoleURL() != null && 
			this.getCreateAccountURL() != null && 
			this.getAccountSeriesText() != null &&
			svcSummary.getServiceMap() != null && 
			svcSummary.getServiceMap().size() > 0) {
	
			return "GOOD";
		}
		else {
			throw new RpcException("Error");
		}
	}

	@Override
	public String getEsbServiceStatusURL() throws RpcException {
		try {
			Properties props = getAppConfig().getProperties(URL_PROPERTIES);
			String esbServiceStatusURL = props.getProperty("esbServiceStatusURL", null);
			if (esbServiceStatusURL == null) {
				throw new RpcException("Null 'esbServiceStatusURL' property.  This application is not configured correctly.");
			}
			return esbServiceStatusURL;
		} catch (EnterpriseConfigurationObjectException e) {
			e.printStackTrace();
			throw new RpcException(e.getMessage());
		}
	}

	@Override
	public String getServiceOverviewURL() throws RpcException {
		try {
			Properties props = getAppConfig().getProperties(URL_PROPERTIES);
			String esbServiceStatusURL = props.getProperty("servicesOverviewURL", null);
			if (esbServiceStatusURL == null) {
				throw new RpcException("Null 'servicesOverviewURL' property.  This application is not configured correctly.");
			}
			return esbServiceStatusURL;
		} catch (EnterpriseConfigurationObjectException e) {
			e.printStackTrace();
			throw new RpcException(e.getMessage());
		}
	}

	@Override
	public String getUseCasesURL() {
		try {
			Properties props = getAppConfig().getProperties(URL_PROPERTIES);
			String esbServiceStatusURL = props.getProperty("useCasesURL", null);
			if (esbServiceStatusURL == null) {
				throw new RpcException("Null 'useCasesURL' property.  This application is not configured correctly.");
			}
			return esbServiceStatusURL;
		} catch (EnterpriseConfigurationObjectException e) {
			e.printStackTrace();
			throw new RpcException(e.getMessage());
		}
	}

	@Override
	public String getGettingStartedURL() {
		try {
			Properties props = getAppConfig().getProperties(URL_PROPERTIES);
			String esbServiceStatusURL = props.getProperty("gettingStartedURL", null);
			if (esbServiceStatusURL == null) {
				throw new RpcException("Null 'gettingStartedURL' property.  This application is not configured correctly.");
			}
			return esbServiceStatusURL;
		} catch (EnterpriseConfigurationObjectException e) {
			e.printStackTrace();
			throw new RpcException(e.getMessage());
		}
	}

	@Override
	public String getArchitectureURL() {
		try {
			Properties props = getAppConfig().getProperties(URL_PROPERTIES);
			String esbServiceStatusURL = props.getProperty("architectureURL", null);
			if (esbServiceStatusURL == null) {
				throw new RpcException("Null 'architectureURL' property.  This application is not configured correctly.");
			}
			return esbServiceStatusURL;
		} catch (EnterpriseConfigurationObjectException e) {
			e.printStackTrace();
			throw new RpcException(e.getMessage());
		}
	}

	@Override
	public String getFaqURL() {
		try {
			Properties props = getAppConfig().getProperties(URL_PROPERTIES);
			String esbServiceStatusURL = props.getProperty("faqURL", null);
			if (esbServiceStatusURL == null) {
				throw new RpcException("Null 'faqURL' property.  This application is not configured correctly.");
			}
			return esbServiceStatusURL;
		} catch (EnterpriseConfigurationObjectException e) {
			e.printStackTrace();
			throw new RpcException(e.getMessage());
		}
	}

	@Override
	public String getSecurityURL() {
		try {
			Properties props = getAppConfig().getProperties(URL_PROPERTIES);
			String esbServiceStatusURL = props.getProperty("securityURL", null);
			if (esbServiceStatusURL == null) {
				throw new RpcException("Null 'securityURL' property.  This application is not configured correctly.");
			}
			return esbServiceStatusURL;
		} catch (EnterpriseConfigurationObjectException e) {
			e.printStackTrace();
			throw new RpcException(e.getMessage());
		}
	}

	@Override
	public String getSupportURL() {
		try {
			Properties props = getAppConfig().getProperties(URL_PROPERTIES);
			String esbServiceStatusURL = props.getProperty("supportURL", null);
			if (esbServiceStatusURL == null) {
				throw new RpcException("Null 'supportURL' property.  This application is not configured correctly.");
			}
			return esbServiceStatusURL;
		} catch (EnterpriseConfigurationObjectException e) {
			e.printStackTrace();
			throw new RpcException(e.getMessage());
		}
	}

	@Override
	public String getPartnersURL() {
		try {
			Properties props = getAppConfig().getProperties(URL_PROPERTIES);
			String esbServiceStatusURL = props.getProperty("partnersURL", null);
			if (esbServiceStatusURL == null) {
				throw new RpcException("Null 'partnersURL' property.  This application is not configured correctly.");
			}
			return esbServiceStatusURL;
		} catch (EnterpriseConfigurationObjectException e) {
			e.printStackTrace();
			throw new RpcException(e.getMessage());
		}
	}

	@Override
	public String getGrantsURL() {
		try {
			Properties props = getAppConfig().getProperties(URL_PROPERTIES);
			String esbServiceStatusURL = props.getProperty("grantsURL", null);
			if (esbServiceStatusURL == null) {
				throw new RpcException("Null 'grantsURL' property.  This application is not configured correctly.");
			}
			return esbServiceStatusURL;
		} catch (EnterpriseConfigurationObjectException e) {
			e.printStackTrace();
			throw new RpcException(e.getMessage());
		}
	}

	@Override
	public String getTrainingURL() {
		try {
			Properties props = getAppConfig().getProperties(URL_PROPERTIES);
			String esbServiceStatusURL = props.getProperty("trainingURL", null);
			if (esbServiceStatusURL == null) {
				throw new RpcException("Null 'trainingURL' property.  This application is not configured correctly.");
			}
			return esbServiceStatusURL;
		} catch (EnterpriseConfigurationObjectException e) {
			e.printStackTrace();
			throw new RpcException(e.getMessage());
		}
	}

	@Override
	public String getFundingURL() {
		try {
			Properties props = getAppConfig().getProperties(URL_PROPERTIES);
			String esbServiceStatusURL = props.getProperty("fundingURL", null);
			if (esbServiceStatusURL == null) {
				throw new RpcException("Null 'fundingURL' property.  This application is not configured correctly.");
			}
			return esbServiceStatusURL;
		} catch (EnterpriseConfigurationObjectException e) {
			e.printStackTrace();
			throw new RpcException(e.getMessage());
		}
	}

	@Override
	public String getResourcesURL() {
		try {
			Properties props = getAppConfig().getProperties(URL_PROPERTIES);
			String esbServiceStatusURL = props.getProperty("resourcesURL", null);
			if (esbServiceStatusURL == null) {
				throw new RpcException("Null 'resourcesURL' property.  This application is not configured correctly.");
			}
			return esbServiceStatusURL;
		} catch (EnterpriseConfigurationObjectException e) {
			e.printStackTrace();
			throw new RpcException(e.getMessage());
		}
	}

	@Override
	public String getWelcomeURL() {
		try {
			Properties props = getAppConfig().getProperties(URL_PROPERTIES);
			String url = props.getProperty("welcomeURL", null);
			if (url == null) {
				throw new RpcException("Null 'welcomeURL' property.  This application is not configured correctly.");
			}
			return url;
		} catch (EnterpriseConfigurationObjectException e) {
			e.printStackTrace();
			throw new RpcException(e.getMessage());
		}
	}

	private boolean toBooleanFromString(String validated) {
		if (validated != null) {
			return Boolean.parseBoolean(validated);
		}

		return false;
	}
	private void setPojoCreateInfo(SharedObject pojo, XmlEnterpriseObject xeo)
			throws XmlEnterpriseObjectException {
		
		java.util.Date d = toDateFromDatetime(((org.openeai.moa.objects.resources.Datetime) xeo
				.getValueFromObject("CreateDatetime")));
		pojo.setCreateInfo((String) xeo.getValueFromObject("CreateUser"), d);
	}

	private void setPojoUpdateInfo(SharedObject pojo, XmlEnterpriseObject xeo)
			throws XmlEnterpriseObjectException {
		
		java.util.Date d = toDateFromDatetime(((org.openeai.moa.objects.resources.Datetime) xeo
				.getValueFromObject("LastUpdateDatetime")));
		pojo.setUpdateInfo((String) xeo.getValueFromObject("LastUpdateUser"), d);
	}
    private java.util.Date toDateFromDatetime(
			org.openeai.moa.objects.resources.Datetime moa) {
		if (moa == null) {
			return null;
		}

		Calendar c = Calendar.getInstance();
		c.setTimeZone(TimeZone.getTimeZone(moa.getTimezone()));

		c.set(Calendar.MONTH, Integer.parseInt(moa.getMonth()) - 1);
		c.set(Calendar.DAY_OF_MONTH, Integer.parseInt(moa.getDay()));
		c.set(Calendar.YEAR, Integer.parseInt(moa.getYear()));
		c.set(Calendar.HOUR_OF_DAY, Integer.parseInt(moa.getHour()));
		c.set(Calendar.MINUTE, Integer.parseInt(moa.getMinute()));
		c.set(Calendar.SECOND, Integer.parseInt(moa.getSecond()));
		c.set(Calendar.MILLISECOND, Integer.parseInt(moa.getSubSecond()));
		return c.getTime();
	}
	private Object getObject(String objectName)
			throws EnterpriseConfigurationObjectException {
		
		return getAppConfig().getObject(objectName);
	}
	private RequestService getAWSRequestService() throws JMSException {
		RequestService reqSvc = (RequestService) awsProducerPool.getProducer();
		((PointToPointProducer) reqSvc)
				.setRequestTimeoutInterval(getDefaultRequestTimeoutInterval());
		return reqSvc;
	}
	public int getDefaultRequestTimeoutInterval() {
		return defaultRequestTimeoutInterval;
	}

	public void setDefaultRequestTimeoutInterval(
			int defaultRequestTimeoutInterval) {
		this.defaultRequestTimeoutInterval = defaultRequestTimeoutInterval;
	}

	@SuppressWarnings("unused")
	private int toIntFromString(String s) {
		if (s == null) {
			return 0;
		} 
		else {
			return Integer.parseInt(s);
		}
	}

	@Override
	public List<PropertiesPojo> getCarouselProperties() throws RpcException {
		try {
			@SuppressWarnings("unchecked")
			List<PropertyConfig> cp_props = (List<PropertyConfig>)getAppConfig().getObjectsLike("CarouselProperties");
			info("got " + cp_props.size() + " CarouselProperties back from AppConfig");
			List<PropertiesPojo> props = new java.util.ArrayList<PropertiesPojo>();
			for (PropertyConfig cpg : cp_props) {
				PropertiesPojo pp = new PropertiesPojo();
				Iterator<Object> keys = cpg.getProperties().keySet().iterator();
				while (keys.hasNext()) {
					String key = (String)keys.next();
					String value = cpg.getProperties().getProperty(key);
					pp.setProperty(key, value);
				}
				props.add(pp);
			}
			Collections.sort(props);
			return props;
		}
		catch (EnterpriseConfigurationObjectException e) {
			e.printStackTrace();
			throw new RpcException(e.getMessage());
		}
	}

	@Override
	public PropertiesPojo getPropertiesForMenu(String menuId) throws RpcException {
		try {
			Properties props = getAppConfig().getProperties(MENU_PROPERTIES + "-" + menuId);
			PropertiesPojo p = new PropertiesPojo();
			Iterator<Object> keys = props.keySet().iterator();
			while (keys.hasNext()) {
				String key = (String)keys.next();
				String value = props.getProperty(key);
				p.setProperty(key, value);
			}
			return p;
		} catch (EnterpriseConfigurationObjectException e) {
			e.printStackTrace();
			throw new RpcException(e.getMessage());
		}
	}

	@Override
	public PropertiesPojo getSiteSpecificTextProperties() throws RpcException {
		try {
			Properties props = getAppConfig().getProperties(SITE_SPECIFIC_TEXT_PROPERTIES);
			PropertiesPojo p = new PropertiesPojo();
			Iterator<Object> keys = props.keySet().iterator();
			while (keys.hasNext()) {
				String key = (String)keys.next();
				String value = props.getProperty(key);
				p.setProperty(key, value);
			}
			return p;
		} catch (EnterpriseConfigurationObjectException e) {
			e.printStackTrace();
			throw new RpcException(e.getMessage());
		}
	}

	@Override
	public String getMonitoringStatusURL() throws RpcException {
		try {
			Properties props = getAppConfig().getProperties(URL_PROPERTIES);
			String url = props.getProperty("monitoringStatusURL", null);
			if (url == null) {
				throw new RpcException("Null 'monitoringStatusURL' property.  This application is not configured correctly.");
			}
			return url;
		} catch (EnterpriseConfigurationObjectException e) {
			e.printStackTrace();
			throw new RpcException(e.getMessage());
		}
	}

}
